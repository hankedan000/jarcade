/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import BirdClimb.Bird;
import BirdClimb.BirdClimbGameInfo;
import BirdClimb.BirdClimbParams;
import static BirdClimb.BirdClimbParams.COIN_GROUP;
import static BirdClimb.BirdClimbParams.GROUND_GROUP;
import static BirdClimb.BirdClimbParams.ROCK_GROUP;
import BirdClimb.Coin;
import BirdClimb.Ground;
import BirdClimb.RockGenerator;
import BirdClimb.ScoreBoard;
import BirdClimb.Sky;
import Game.Data.GameData;
import Game.Data.GameInfo;
import Game.Draw.ResizeEvent;
import Game.JGame;
import Game.Physics.CollisionEngine;
import Game.Physics.CollisionGroup;
import Game.Physics.PhysicsEngine;
import Game.Primitives.Vector2D;
import Game.Services.GameServices;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniel
 */
public class GameMain extends JGame
                           implements BirdClimbParams
{
    private final Sky sky1;
    private final Sky sky2;
    private final List<Bird> players;
    private final Bird bird1;
    private final Bird bird2;
    private final ScoreBoard scoreBoard;
    private final Ground ground;
    private final RockGenerator rockGen;
    
    public GameMain()
    {
        super();
        
        sky1 = new Sky(this);
        sky2 = new Sky(this);
        rockGen = new RockGenerator(this);
        players = new ArrayList<>();
        
        bird1 = new Bird(this,sky1.getViewPort());
        bird1.setColor(Color.RED);
        bird2 = new Bird(this,sky2.getViewPort());
        bird2.setColor(Color.BLACK);
        scoreBoard = new ScoreBoard(this, bird1, bird2);
        ground = new Ground();
        players.add(bird1);
        players.add(bird2);
        
        addObject(ground);
        addObject(bird1);
        addObject(bird2);
        addObject(new Coin());
        addDrawable(rockGen);
        
        addGamePanel(sky1);
        addGamePanel(sky2);
    }

    @Override
    public void
    boot(
            GameServices gs)
    {
        super.boot(gs);
        
        GameData gd = gs.getGameDataService().load(getGameInfo());
        bird1.init(rockGen.getRockList(),gd.getPlayer1Controls().BTN1);
        bird2.init(rockGen.getRockList(),gd.getPlayer2Controls().BTN1);
        addKeyListener(bird1.getFlapListener());
        addKeyListener(bird2.getFlapListener());
        
        sky1.init(scoreBoard, bird1);
        sky2.init(scoreBoard, bird2);
        sky2.getViewPort().setRotateAngle(Math.PI);
        
        PhysicsEngine physicsEngine = getPhysicsEngine();
        physicsEngine.setGravity(new Vector2D(0,getGravity()));
        physicsEngine.registerOnSimListener(scoreBoard);
        physicsEngine.registerOnSimListener(rockGen);
        physicsEngine.registerOnSimListener(sky1);
        physicsEngine.registerOnSimListener(sky2);
        physicsEngine.registerOnSimListener(bird1);
        physicsEngine.registerOnSimListener(bird2);
        
        // Make the collision groups
        CollisionEngine collEng = getCollisionEngine();
        CollisionGroup groundCG = collEng.getCollisionGroup(GROUND_GROUP);
        groundCG.setOnlyCollideNoneSimilar(true);
        groundCG.add(ground);
        groundCG.addAll((List)players);
        
        CollisionGroup rockCG = collEng.getCollisionGroup(ROCK_GROUP);
        rockCG.setOnlyCollideNoneSimilar(true);
        rockCG.add(bird1);
        rockCG.addAll((List)players);
        
        CollisionGroup coinCG = collEng.getCollisionGroup(COIN_GROUP);
        rockCG.setOnlyCollideNoneSimilar(true);
        coinCG.add(bird1);
        coinCG.addAll((List)players);
        
        rockGen.init(rockCG,this);
    }
    
    @Override
    public GameInfo getGameInfo()
    {
        return new BirdClimbGameInfo();
    }

//    @Override
//    public void
//    keyPressed(
//            KeyEvent ke)
//    {
//        if(ke.getKeyCode() == KeyEvent.VK_ESCAPE)
//        {
//            shutdown();
////            this.dispose();
//        }
//        else if(ke.getKeyCode() == KeyEvent.VK_P)
//        {
//            if(isPaused())
//            {
//                resume();
//            }
//            else
//            {
//                pause();
//            }
//        }
//    }

    @Override
    public int
    getGroundTop()
    {
        return ground.getTop();
    }

    @Override
    public int
    getStartHeight()
    {
        return 0;
    }

    @Override
    public int
    getAltitudePerPoint()
    {
        return getGroundWidth() / 10;
    }

    @Override
    public int
    getGravity()
    {
        return (int)(getGroundWidth() * 3.5);
    }

    @Override
    public int
    getFurthestHeight()
    {
        int maxHeight = 0;
        for(Bird player : players)
        {
            maxHeight = Math.min(maxHeight, (int)player.getPosition().dY);
        }
        return maxHeight;
    }

    @Override
    public boolean
    areAllBirdsDead()
    {
        for(Bird bird : players)
        {
            if(!bird.isDead())
            {
                return false;
            }
        }
        
        return true;
    }

    @Override
    public boolean
    anyBirdsStillFlying()
    {
        for(Bird bird : players)
        {
            if(bird.isFlying())
            {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public void
    notifyResize(
            ResizeEvent re)
    {
        super.notifyResize(re);
        
        getPhysicsEngine().setGravity(new Vector2D(0,getGravity()));
    }
    
    private int
    getGroundWidth()
    {
        return Math.max(sky1.getWidth(),sky2.getWidth());
    }
}
