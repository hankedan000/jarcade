/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Draw.ResizeEvent;
import Game.Draw.ViewPort;
import Game.Primitives.GameObject;
import Game.Utilities.ShapeUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author daniel
 */
public class Coin extends GameObject
{
    private static final Color COIN_COLOR = Color.YELLOW;
    private Ellipse2D body;
    
    public Coin()
    {
        // Coins are fixed in place
        setStatic(true);
    }
    
    @Override
    protected Area
    getLocalBounds()
    {
        return new Area(body);
    }

    @Override
    public void
    draw(
            Graphics2D g2d)
    {
        AffineTransform oldTrans = g2d.getTransform();
        g2d.translate(s_.dX, s_.dY);
        g2d.setColor(COIN_COLOR);
        g2d.fill(body);
        g2d.setTransform(oldTrans);
    }

    @Override
    public void
    onResize(
            ResizeEvent re)
    {
        int skyWidth = re.dimensions[0].width;
        int coinW = skyWidth / 20;
        boolean firstResize = body == null;
        double oldW = 0;
        
        if (!firstResize)
        {
            oldW = body.getWidth();
        }
        
        body = ShapeUtils.getEllipse(
                coinW,
                coinW,
                ViewPort.OriginPosition.eCentered);
        
        if (!firstResize && oldW != 0)
        {
            double scale = body.getWidth() / oldW;
            s_ = s_.scale(scale);
        }
        
        // Only once we have a body can we become collidable
        setCollidable(true);
    }
    
}
