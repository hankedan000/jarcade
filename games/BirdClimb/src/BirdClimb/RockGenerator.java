/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import BirdClimb.Rock.Side;
import Game.Draw.Drawable;
import Game.Draw.ResizeEvent;
import Game.Physics.CollisionGroup;
import Game.Physics.OnSimListener;
import Game.SpawnerInterface;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author daniel
 */
public class RockGenerator implements OnSimListener,
                                      Drawable
{
    private final BirdClimbParams gameInfo_;
    private final LinkedList<Rock> rocks;
    private CollisionGroup rockCG;
    private SpawnerInterface spawner;
    private int skyWidth;
    private int skyHeight;
    private int minRockSpacing;
    private int prevInsertHeight;
    private Random rand;
    
    public RockGenerator(
            BirdClimbParams gameInfo)
    {
        gameInfo_ = gameInfo;
        rocks = new LinkedList<>();
        skyWidth = -1;
        rand = new Random();
    }
    
    public void
    init(
            CollisionGroup rockGroup,
            SpawnerInterface spawnerInterface)
    {
        rockCG  = rockGroup;
        spawner = spawnerInterface;
    }
    
    public LinkedList<Rock>
    getRockList()
    {
        return rocks;
    }
    
    private int
    quantize(
            int height)
    {
        return (int)Math.ceil(height / minRockSpacing) * minRockSpacing;
    }
    
    @Override
    public void
    onSimBegin()
    {
        // do nothing
    }

    @Override
    public void
    onSimComplete()
    {
        if (skyWidth > 0)
        {
            int newInsertHeight = quantize(gameInfo_.getFurthestHeight());
            // increment large enough to push off top of screen
            newInsertHeight -= quantize(skyHeight);

            if(prevInsertHeight > newInsertHeight)
            {// we need to generate new rocks
                insertNewRocks(prevInsertHeight, newInsertHeight);

                prevInsertHeight = newInsertHeight;
            }
        }
    }
    
    private void
    insertNewRocks(
            int lowerHeight,
            int upperHeight)
    {
        int nextInsertHeight = lowerHeight;
        int maxToGen = (lowerHeight - upperHeight) / minRockSpacing;
        
        for(int r=0; r<maxToGen; r++)
        {
            Side side = (rand.nextBoolean() ? Side.eLEFT : Side.eRIGHT);
            Rock newRock = new Rock(skyWidth,
                                    nextInsertHeight,
                                    side);
            rocks.add(newRock);
            rockCG.add(newRock);
            spawner.addObject(newRock);
            
            nextInsertHeight -= minRockSpacing;
        }
    }

    @Override
    public void draw(Graphics2D g2d)
    {
        g2d.setPaint(Color.WHITE);
        g2d.drawLine(skyWidth / -2, prevInsertHeight,
                     skyWidth /  2, prevInsertHeight);
    }

    @Override
    public void
    onResize(
            ResizeEvent re)
    {
        boolean firstResize = skyWidth == -1;
        int oldSkyWidth = skyWidth;
        skyWidth = (int)re.dimensions[0].getWidth();
        skyHeight = (int)re.dimensions[0].getHeight();
        
        if (firstResize)
        {
            prevInsertHeight  = gameInfo_.getStartHeight();
            prevInsertHeight -= skyWidth;
        }
        else
        {
            double scale = (double)skyWidth / oldSkyWidth;
            prevInsertHeight *= scale;
        }
        minRockSpacing = skyWidth * 3 / 10;
    }
    
}
