/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Primitives.Vector2D;
import java.awt.event.KeyEvent;

/**
 *
 * @author daniel
 */
public interface BirdClimbParams
{
    static final String ROCK_GROUP   = "ROCK";
    static final String COIN_GROUP   = "COIN";
    static final String GROUND_GROUP = "GROUND";
    
    static final long BIRD_COLLIDABLE_ID   = 0;
    static final long ROCK_COLLIDABLE_ID   = 1;
    static final long GROUND_COLLIDABLE_ID = 2;
    
    static final int PLAYER1_FLAP_CODE = KeyEvent.VK_SPACE;
    static final int PLAYER2_FLAP_CODE = KeyEvent.VK_ENTER;
    
    int
    getGroundTop();
    
    int
    getStartHeight();
    
    int
    getAltitudePerPoint();
    
    int
    getGravity();
    
    int
    getFurthestHeight();
    
    boolean
    areAllBirdsDead();
    
    boolean
    anyBirdsStillFlying();
}
