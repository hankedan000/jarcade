/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import static BirdClimb.BirdClimbParams.GROUND_COLLIDABLE_ID;
import static BirdClimb.BirdClimbParams.ROCK_COLLIDABLE_ID;
import BirdClimb.FlapListener.OnFlapListener;
import Game.Draw.ResizeEvent;
import Game.Draw.ViewPort;
import Game.JGame;
import Game.Utilities.ShapeUtils;
import Game.Physics.CollisionEvent;
import Game.Physics.OnSimListener;
import Game.Primitives.GameObject;
import Game.Primitives.Vector2D;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author daniel
 */
public class Bird extends GameObject
                  implements OnSimListener,
                             OnFlapListener
{
    private final ViewPort vp_;
    private final BirdClimbParams gameInfo_;
    private final BirdDeathTimeout deathTimeout;
    private final RockCollisionWindow collisionWindow;
    private FlapListener flapListener;
    private Polygon body;
    private Color c;
    private boolean dead;
    private long altitude;
    private int skyWidth;

    public Bird(
            BirdClimbParams gameInfo,
            ViewPort vp)
    {
        setCollidableID(BirdClimbParams.BIRD_COLLIDABLE_ID);

        vp_ = vp;
        gameInfo_ = gameInfo;
        deathTimeout = new BirdDeathTimeout();
        collisionWindow = new RockCollisionWindow();
        
        c = new Color(255,0,0);
        dead = false;
    }
    
    public void
    init(
            LinkedList<Rock> rockList,
            int flapKeyCode)
    {
        collisionWindow.init(rockList);
        flapListener = new FlapListener(this, flapKeyCode);
    }
    
    public long
    getAltitude()
    {
        return altitude;
    }
    
    public FlapListener
    getFlapListener()
    {
        return flapListener;
    }
    
    public void
    setColor(
            Color color)
    {
        c = color;
    }
    
    public boolean
    isDead()
    {
        return dead;
    }
    
    public boolean
    isFlying()
    {
        return !dead && s_.dY > gameInfo_.getGroundTop() + 10;
    }
    
    public int
    getHeight()
    {
        return body.getBounds().height;
    }
    
    public int
    getWidth()
    {
        return body.getBounds().width;
    }
    
    /**
     * Will draw the Car in a Graphics2D object
     * @param g2d the Graphics2D object to draw in
     */
    @Override
    public void
    draw(
            Graphics2D g2d)
    {
        AffineTransform old = g2d.getTransform();// save transform
        
        drawBody(g2d);
        g2d.rotate(v_.angle(),s_.dX,s_.dY);
        
        g2d.setTransform(old);// restore transform
    }

    @Override
    public void
    onSimBegin()
    {
        collisionWindow.onSimBegin();
    }

    @Override
    public void
    onSimComplete()
    {
        Area areaOutSideOfView = new Area(getGlobalBounds());
        areaOutSideOfView.subtract(vp_.getGlobalBounds());
        
        if(!areaOutSideOfView.isEmpty())
        {
            v_.dX = v_.dX * -1;
        }
        
        altitude = (long)s_.dY * -1;
        
        collisionWindow.onSimComplete();
    }

    @Override
    public void
    onShutdown()
    {
        deathTimeout.kill();
    }

    @Override
    public void
    onFlap()
    {
        if(!dead)
        {
            v_.dY = getFlapVelocity();
        }
        else if(!gameInfo_.anyBirdsStillFlying() && deathTimeout.timedout())
        {
            resetToStart();
            v_.dY = getFlapVelocity();
        }
    }

    @Override
    public void
    onResize(
            ResizeEvent re)
    {        
        skyWidth = re.dimensions[0].width;
        
        // Sometimes it's zero... lets wait till it's positive
        if (skyWidth == 0)
        {
            return;
        }
        
        int birdW = skyWidth / 10;
        int birdH = birdW * 8 / 10;
        boolean firstResize = body == null;
        double oldW = 0;
        
        if (firstResize)
        {
            // Get a random starting velocity direction
            int startVelSign = (JGame.rand.nextBoolean() ? -1 : 1);
            v_ = new Vector2D(startVelSign,0);
            
            resetToStart();
        }
        else
        {
            oldW = getWidth();
        }
        
        // Make the body
        body = ShapeUtils.getPolyRectangle(
                birdW,
                birdH,
                ViewPort.OriginPosition.eCentered);
        
        // Set X velocity so it takes bird 1 second to cross the screen
        v_.dX = Math.copySign(skyWidth, v_.dX);
        
        // Scale position
        if (!firstResize && oldW != 0)
        {
            double scale = getWidth() / oldW;
            s_ = s_.scale(scale);
        }
        
        // Only once we have a body can we become collidable
        setCollidable(true);
    }
    
    private void
    drawBody(
            Graphics2D g2d)
    {
        // Make a copy so that we can translate it to it's proper position
        Polygon tb = new Polygon(body.xpoints,body.ypoints,body.npoints);
        tb.translate((int)s_.dX, (int)s_.dY);
        g2d.setPaint(c);
        g2d.fillPolygon(tb);
    }
    
    private void
    resetToStart()
    {
        // Pick a random startX that is within inner 70% of sky width
        int startX = JGame.rand.nextInt(skyWidth);
        startX -= skyWidth / 2;
        startX *= 0.7;
        s_ = new Vector2D(startX,gameInfo_.getStartHeight());
        
        dead = false;
    }
    
    private int
    getFlapVelocity()
    {
        return (int)(skyWidth * -1.4);
    }
    
    @Override
    protected Area
    getLocalBounds()
    {
        return new Area(body);
    }

    @Override
    protected void
    onCollisionEnter(
            CollisionEvent ce)
    {
        if(!dead &&
            ce.other.getCollidableID() == ROCK_COLLIDABLE_ID)
        {
            dead = true;
            deathTimeout.reset();
            v_.dX = 0;
        }
    }

    @Override
    protected void
    onCollisionExit(
            CollisionEvent ce)
    {
        // do nothing
    }

    @Override
    protected void
    onCollisionEvent(
            CollisionEvent ce)
    {
        if(ce.other.getCollidableID() == GROUND_COLLIDABLE_ID)
        {
            Ground ground = (Ground)ce.other;
            s_.dY = ground.getTop()- getHeight() / 2;
            v_.dY = 0;
        }
    }
    
    private class BirdDeathTimeout
    {
        private final Timer timer;
        private TimeoutTask timeout = new TimeoutTask();
        
        public BirdDeathTimeout()
        {
            timer = new Timer("BirdDeathTimeoutTimer");
        }
        
        public boolean
        timedout()
        {
            return timeout.timedout;
        }
        
        public void
        reset()
        {
            timeout = new TimeoutTask();
            timer.schedule(timeout, 1000);// wait for 1 second
        }
        
        public void
        kill()
        {
            // cancel the timer so that we don't hang the thread
            timer.cancel();
        }

        private class TimeoutTask extends TimerTask
        {
            public boolean timedout = false;

            @Override
            public void run()
            {
                timedout = true;
                cancel();
            }
        }
    }
}
