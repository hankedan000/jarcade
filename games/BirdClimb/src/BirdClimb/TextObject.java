/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Draw.ResizeEvent;
import java.awt.Graphics2D;
import Game.Primitives.GameObject;
import java.awt.Font;
import java.awt.geom.Area;

/**
 *
 * @author daniel
 */
public class TextObject extends GameObject
{
    String text;
    
    public TextObject()
    {
        setCollidable(false);
    }
    
    public void
    setText(
            String text)
    {
        this.text = text;
    }
    
    @Override
    protected Area getLocalBounds()
    {
        // do nothing
        return null;
    }

    @Override
    public void draw(Graphics2D g2d)
    {
        if(text != null)
        {
            g2d.drawString(text, (int)s_.dX, (int)s_.dY);
        }
    }

    @Override
    public void
    onResize(
            ResizeEvent re)
    {
        // do nothing
    }
    
}
