/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

/**
 *
 * @author daniel
 */
public interface ScoreBoardInterface
{
    long getPlayer1Score();

    long getPlayer2Score();
}
