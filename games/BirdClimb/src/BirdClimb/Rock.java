/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Draw.ResizeEvent;
import Game.JGame;
import Game.Draw.ViewPort;
import Game.Primitives.GameObject;
import Game.Primitives.Vector2D;
import Game.Utilities.GMath;
import Game.Utilities.ShapeUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author daniel
 */
public class Rock extends GameObject
{
    public enum Side
    {
        eLEFT,eRIGHT
    }
    private final Side side;
    private final int initSkyWidth;
    private final Vector2D initPosition;
    private int bodyW_;
    private Polygon initBody;
    private Polygon drawnBody;
    private Ellipse2D initEye;
    private Ellipse2D drawnEye;

    public Rock(
            int skyWidth,
            int height,
            Side side)
    {
        setCollidableID(BirdClimbParams.ROCK_COLLIDABLE_ID);
        setCollidable(true);
        setStatic(true);
        
        this.side = side;
        initSkyWidth = skyWidth;
        initPosition = new Vector2D();
        
        initPosition.dY = height;
        computeInitGeometries(skyWidth);
    }
    
    @Override
    protected Area
    getLocalBounds()
    {
        return new Area(drawnBody);
    }

    @Override
    public void
    draw(
            Graphics2D g2d)
    {
        // Make a copy so that we can translate it to it's proper position
        Polygon tb = new Polygon(drawnBody.xpoints,drawnBody.ypoints,drawnBody.npoints);
        tb.translate((int)s_.dX, (int)s_.dY);
        g2d.setPaint(Color.LIGHT_GRAY);
        g2d.fillPolygon(tb);
        
        drawEye(g2d);
    }
    
    private void
    drawEye(
            Graphics2D g2d)
    {
        AffineTransform oldTrans = g2d.getTransform();
        int eyeCenterY = (int)s_.dY - bodyW_ / 4;
        int eyeCenterX = (int)s_.dX + (side == Side.eLEFT ?
                                       bodyW_ / 4         :
                                       bodyW_ / -4);
        
        // Draw iris
        g2d.translate(eyeCenterX,eyeCenterY);
        g2d.setPaint(Color.WHITE);
        g2d.fill(drawnEye);
        
        // Draw pupil
        g2d.setPaint(Color.BLACK);
        g2d.scale(-0.3, -0.3);
        g2d.fill(drawnEye);
        g2d.setTransform(oldTrans);
    }
    
    private void
    computeInitGeometries(
            int skyWidth)
    {
        double randWidthScale = 
                GMath.map(JGame.rand.nextInt(50), 0, 50, -0.25, 0.25);
        bodyW_  = skyWidth * 5 / 100;
        bodyW_ += bodyW_ * randWidthScale;
        int leftInsertX  = skyWidth / -2 + bodyW_ / 2;
        int rightInsertX = skyWidth /  2 - bodyW_ / 2;
        
        // Make initial body to base future scaled bodies from
        initBody = new Polygon();
        int deform = bodyW_ / 2;
        int deformHalf = deform / 2;
        if(side == Side.eLEFT)
        {
            initBody.addPoint(bodyW_ / -2, bodyW_ / -2);// upper wall point
            initBody.addPoint(bodyW_ / -2, bodyW_ /  2);// lower wall point
            initBody.addPoint(bodyW_ /  2 + JGame.rand.nextInt(deform) - deformHalf,
                           bodyW_ /  2 + JGame.rand.nextInt(deform) - deformHalf);// lower air point
            initBody.addPoint(bodyW_ /  2 + JGame.rand.nextInt(deform) - deformHalf,
                           bodyW_ / -2 + JGame.rand.nextInt(deform) - deformHalf);// upper air point
        }
        else
        {
            initBody.addPoint(bodyW_ / 2,  bodyW_ / -2);// upper wall point
            initBody.addPoint(bodyW_ / 2,  bodyW_ /  2);// lower wall point
            initBody.addPoint(bodyW_ / -2 + JGame.rand.nextInt(deform) - deformHalf,
                           bodyW_ /  2 + JGame.rand.nextInt(deform) - deformHalf);// lower air point
            initBody.addPoint(bodyW_ / -2 + JGame.rand.nextInt(deform) - deformHalf,
                           bodyW_ / -2 + JGame.rand.nextInt(deform) - deformHalf);// upper air point
        }
        
        // Make initial eye to base future scaled eyes from
        initEye = ShapeUtils.getEllipse(
                bodyW_ / 4,
                bodyW_ / 4,
                ViewPort.OriginPosition.eCentered);
        
        initPosition.dX = (side == Side.eLEFT ? leftInsertX : rightInsertX);
        
        // Put initial body into the body we use to draw
        drawnBody = new Polygon(
                initBody.xpoints,
                initBody.ypoints,
                initBody.npoints);
        
        // Put initial eye into the eye we use to draw
        drawnEye = ShapeUtils.getEllipse(
                (int)initEye.getWidth(),
                (int)initEye.getHeight(),
                ViewPort.OriginPosition.eCentered);
        
        // Set position to initPosition
        s_.dX = initPosition.dX;
        s_.dY = initPosition.dY;
    }
    
    @Override
    public void
    onResize(
            ResizeEvent re)
    {
        int skyWidth = (int)re.dimensions[0].getWidth();
        double scale = (double)skyWidth / initSkyWidth;
        
        // Scale drawn body from the original
        drawnBody.reset();
        for(int p=0; p<initBody.npoints; p++)
        {
            drawnBody.addPoint(
                    (int)Math.ceil(initBody.xpoints[p] * scale),
                    (int)Math.ceil(initBody.ypoints[p] * scale));
        }
        
        // Scale drawn eye from the original
        drawnEye = ShapeUtils.getEllipse(
                (int)(initEye.getWidth() * scale),
                (int)(initEye.getHeight() * scale),
                ViewPort.OriginPosition.eCentered);
        
        // Scale position from the original
        s_ = initPosition.scale(scale);
    }
}
