/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Draw.ResizeEvent;
import Game.Draw.ViewPort;
import Game.Primitives.GameObject;
import Game.Utilities.ShapeUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author daniel
 */
public class Ground extends GameObject
{
    private Rectangle2D body;
    
    public Ground()
    {
        setCollidableID(BirdClimbParams.GROUND_COLLIDABLE_ID);
        setStatic(true);
    }

    /**
     * @return 
     * The Y position of the grounds top surface
     */
    public int
    getTop()
    {
        if (body == null)
        {
            return 0;
        }
        else
        {
            return (int)(body.getWidth() / 2);
        }
    }
    
    @Override
    protected Area
    getLocalBounds()
    {
        return new Area(body);
    }

    @Override
    public void
    draw(
            Graphics2D g2d)
    {
        AffineTransform oldTrans = g2d.getTransform();
        g2d.translate(s_.dX, s_.dY);
        g2d.setColor(Color.GREEN);
        g2d.fill(body);
        g2d.setTransform(oldTrans);
    }

    @Override
    public void
    onResize(
            ResizeEvent re)
    {
        int bodyW = re.dimensions[0].width;
        int bodyH = re.dimensions[0].height / 2;
        
        // Make the body
        body = ShapeUtils.getRectangle(
                bodyW,
                bodyH,
                ViewPort.OriginPosition.eCentered);
        
        // Set position
        s_.dY = bodyH / 2 + getTop();
        
        // Only once we have a body can we become collidable
        setCollidable(true);
    }
    
}
