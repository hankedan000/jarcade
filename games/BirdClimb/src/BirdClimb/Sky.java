/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Draw.GamePanel;
import Game.Draw.ViewPort;
import Game.Physics.OnSimListener;
import Game.Primitives.Vector2D;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.geom.AffineTransform;
import Game.Draw.RendererInterface;

/**
 *
 * @author daniel
 */
public class Sky extends GamePanel implements OnSimListener,
                                           ComponentListener
{   
    private final TextObject p1ScoreText;
    private final TextObject p2ScoreText;
    private ScoreBoardInterface sbi;
    Bird trackingPlayer;
    
    public Sky(
            RendererInterface rendererInterface)
    {
        super(rendererInterface);
        
        p1ScoreText = new TextObject();
        p2ScoreText = new TextObject();
        viewport.setOriginPosition(ViewPort.OriginPosition.eCentered);
        
        setBackground(Color.CYAN);
        addComponentListener(this);
    }
    
    public void
    init(
            ScoreBoardInterface scoreBoardInterface,
            Bird player)
    {
        sbi = scoreBoardInterface;
        trackingPlayer = player;
    }
    
    @Override
    protected void
    paintComponent(
            Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        
        // Draw origin for debugging purposes
        drawOrigin(g2d);
        
        // Nullify the transform so score's are with respect to JPanel
        AffineTransform clearedTrans = viewport.getLocalTransform();
        g2d.setTransform(clearedTrans);
        if(sbi != null)
        {
            p1ScoreText.setText(Long.toString(sbi.getPlayer1Score()));
            p2ScoreText.setText(Long.toString(sbi.getPlayer2Score()));
        }
        p1ScoreText.draw(g2d);
        p2ScoreText.draw(g2d);
        
        // Reduces animation jitter in Linux
        Toolkit.getDefaultToolkit().sync();
    }
    
    private
    void drawOrigin(Graphics2D g2d)
    {
        // Draw crosshair where (0,0) is
        g2d.setColor(Color.BLACK);
        g2d.drawLine(-100, 0, 100, 0);
        g2d.drawLine(0, 100, 0, -100);
        
        Polygon arrow = new Polygon();
        arrow.addPoint(4, 100);
        arrow.addPoint(-4, 100);
        arrow.addPoint(0, 105);
        g2d.setColor(Color.YELLOW);
        g2d.fill(arrow);
    }

    @Override
    public void
    onSimBegin()
    {
        // don't care
    }

    @Override
    public void
    onSimComplete()
    {
        if(trackingPlayer != null)
        {
            Vector2D newOrigin = viewport.getOrigin();
            newOrigin.dY = trackingPlayer.getPosition().dY;
            viewport.setOrigin(newOrigin);
        }
    }

    @Override
    public void
    componentResized(
            ComponentEvent ce)
    {
        int halfW = getWidth()  / 2;
        int halfH = getHeight() / 2;
        
        p1ScoreText.setPosition(new Vector2D(-halfW,
                                             -halfH + 15));
        p2ScoreText.setPosition(new Vector2D( halfW - 60,
                                             -halfH + 15));
    }

    @Override
    public void componentMoved(ComponentEvent ce) {
    }

    @Override
    public void componentShown(ComponentEvent ce) {
    }

    @Override
    public void componentHidden(ComponentEvent ce) {
    }
}
