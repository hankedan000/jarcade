/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.JGame;
import Game.Draw.Drawable;
import Game.Physics.OnSimListener;
import Game.Primitives.Vector2D;
import java.awt.Graphics2D;

/**
 *
 * @author daniel
 */
public class ScoreBoard implements OnSimListener,
                                   ScoreBoardInterface
{
    private final BirdClimbParams gameInfo;
    private final Bird p1;
    private final Bird p2;
    private long p1AltScore;
    private long p2AltScore;
    
    public ScoreBoard(
            BirdClimbParams gameInfo,
            Bird player1,
            Bird player2)
    {
        this.gameInfo = gameInfo;
        p1 = player1;
        p2 = player2;
        
        resetScore();
    }
    
    public final void
    resetScore()
    {
        p1AltScore = 0;
        p2AltScore = 0;
    }
    
    private long
    altitudeToScore(
            long alt)
    {
        int altUnit = gameInfo.getAltitudePerPoint();
        if(alt<=0)
        {
            return 0;
        }
        else
        {
            return (alt / altUnit) * altUnit;
        }
    }
    
    @Override
    public void
    onSimBegin()
    {
        // do nothing
    }

    @Override
    public void
    onSimComplete()
    {
        p1AltScore = Math.max(p1AltScore,
                              altitudeToScore(p1.getAltitude()));
        p2AltScore = Math.max(p2AltScore,
                              altitudeToScore(p2.getAltitude()));
    }

    @Override
    public long
    getPlayer1Score()
    {
        return p1AltScore;
    }

    @Override
    public long
    getPlayer2Score()
    {
        return p2AltScore;
    }
}
