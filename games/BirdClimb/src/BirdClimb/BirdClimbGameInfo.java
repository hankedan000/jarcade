/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Data.GameInfo;
import Game.Draw.PanelPlacement;
import java.awt.Color;

/**
 *
 * @author daniel
 */
public class BirdClimbGameInfo implements GameInfo
{
    @Override
    public String
    getName()
    {
        return "BirdClimb";
    }

    @Override
    public PanelPlacement
    getPanelPlacement()
    {
        return PanelPlacement.eSideBySide;
    }

    @Override
    public double getAspectRatio() {
        return 3.0/4.0;
    }

    @Override
    public Color getBezelColor() {
        return Color.LIGHT_GRAY;
    }

    @Override
    public int
    minPlayers()
    {
        return 1;
    }

    @Override
    public int
    maxPlayers()
    {
        return 2;
    }
}
