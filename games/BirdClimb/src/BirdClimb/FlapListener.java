/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author daniel
 */
public class FlapListener implements KeyListener
{
    private final OnFlapListener listener;
    private final int flapKeyCode;
    private boolean flapReleased;
    
    public FlapListener(
            OnFlapListener onFlapListener,
            int keyCode)
    {
        listener = onFlapListener;
        flapKeyCode = keyCode;
        flapReleased = true;
    }

    private boolean
    isMyKeyCode(
            KeyEvent ke)
    {
        return ke.getKeyCode() == flapKeyCode;
    }
    
    @Override
    public void
    keyTyped(
            KeyEvent ke)
    {
        // do nothing
    }
    
    @Override
    public void
    keyPressed(
            KeyEvent ke)
    {
        if(flapReleased && isMyKeyCode(ke))
        {
            listener.onFlap();
            flapReleased = false;
        }
    }

    @Override
    public void
    keyReleased(
            KeyEvent ke)
    {
        if(!flapReleased && isMyKeyCode(ke))
        {
            flapReleased = true;
        }
    }
    
    public interface OnFlapListener
    {
        void onFlap();
    }
}
