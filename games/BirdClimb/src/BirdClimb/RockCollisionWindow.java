/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BirdClimb;

import Game.Physics.OnSimListener;
import java.util.LinkedList;

/**
 *
 * @author daniel
 */
public class RockCollisionWindow implements OnSimListener
{
    LinkedList<Rock> rocks;
    
    public RockCollisionWindow()
    {
    }
    
    public void
    init(
            LinkedList<Rock> rockList)
    {
        rocks = rockList;
    }
    
    @Override
    public void onSimBegin()
    {
    }

    @Override
    public void onSimComplete()
    {
        if (rocks != null)
        {
            System.out.println(rocks.size());
        }
    }
    
}
