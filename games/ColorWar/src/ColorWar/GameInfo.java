/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ColorWar;

import Game.Draw.PanelPlacement;
import java.awt.Color;

/**
 *
 * @author daniel
 */
public class GameInfo implements Game.Data.GameInfo{

    @Override
    public String getName() {
        return "Color War";
    }

    @Override
    public PanelPlacement getPanelPlacement() {
        return PanelPlacement.eSingle;
    }

    @Override
    public double getAspectRatio() {
        return 3.0/4.0;
    }

    @Override
    public Color getBezelColor() {
        return Color.BLACK;
    }

    @Override
    public int minPlayers() {
        return 1;
    }

    @Override
    public int maxPlayers() {
        return 2;
    }
    
}
