/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ColorWar;

import Game.Draw.RendererInterface;
import Game.Draw.ViewPort;
import Game.Utilities.ShapeUtils;
import Game.Data.PlayerControls.ArcadeButton;
import Game.Draw.Drawable;
import Game.Draw.ResizeEvent;
import Game.GameStateListener;
import Game.Primitives.Vector2D;
import Game.Utilities.Stopwatch;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author daniel
 */
public class GamePanel extends Game.Draw.GamePanel implements Drawable,
                                                              GameStateListener{
    Grid grid;
    List<Player> players;
    int pulseSize;
    int pulseRate;
    Shape dot;
    Shape bridge;
    
    Stopwatch sw;
    static final String DRAW_RECORD = "GamePanel.Draw";
    
    public GamePanel(RendererInterface renderer) {
        super(renderer);
        players = new ArrayList<>();
        pulseSize = 20;
        pulseRate = 2;
        
        viewport.setOriginPosition(ViewPort.OriginPosition.eUpperLeft);
        setBackground(new Color(0xdbebff));
        
        sw = new Stopwatch();
        sw.createRecord(DRAW_RECORD);
    }
    
    public void setGrid(Grid g) {
        grid = g;
    }
    
    public void addPlayer(Player p) {
        players.add(p);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        sw.start(DRAW_RECORD);
        if (grid == null) {
            return;
        }
        
        int size = grid.getSize();
        Graphics2D g2d = (Graphics2D)g;
        
        Cell c = new Cell(0, 0);
        for (c.x=0; c.x<size; c.x++) {
            for (c.y=0; c.y<size; c.y++) {
                drawDot(g2d, grid, 1, c, Utils.getColor(grid.getValue(c.x, c.y)));
            }
        }
        
        drawHelp(g2d);
        drawBridges(g2d, grid);
        sw.stop(DRAW_RECORD);
        
//        for (Player p : players) {
//            drawPlayerCaptures(g2d, p);
//        }
//        drawPulse(g2d);
    }
    
    private void drawPulse(Graphics2D g2d) {
        if (pulseSize > getWidth() || pulseSize < 20) {
            pulseRate *= -1;
        }
        pulseSize += pulseRate;
        
        Shape circle = ShapeUtils.getRectangle(pulseSize, pulseSize, ViewPort.OriginPosition.eCentered);
        AffineTransform af = new AffineTransform();
        af.translate(getWidth()/2, getHeight()/2);
        circle = af.createTransformedShape(circle);
        
        g2d.setPaint(Color.RED);
        g2d.fill(circle);
    }
    
    private void drawBridges(Graphics2D g2d, Grid g) {
        int s = grid.getSize();
        Cell c1 = new Cell(0, 0);
        Cell c2 = new Cell(0, 0);
        for (c1.x=0; c1.x<s; c1.x++) {
            for (c1.y=0; c1.y<s; c1.y++) {
                if (c1.x < (s - 1)) {
                    c2.x = c1.x + 1;
                    c2.y = c1.y;
                    if (grid.getValue(c1) == grid.getValue(c2)) {
                        drawBridge(g2d, g, c1, c2);
                    }
                }
                
                if (c1.y < (s - 1)) {
                    c2.x = c1.x;
                    c2.y = c1.y + 1;
                    if (grid.getValue(c1) == grid.getValue(c2)) {
                        drawBridge(g2d, g, c1, c2);
                    }
                }
            }
        }
    }
    
    private void drawPlayerCaptures(Graphics2D g2d, Player p) {
        Set<Cell> caps = p.getCaptures();
        for (Cell c : caps) {
            drawDot(g2d, grid, 0.5, c, Color.BLACK);
        }
    }
    
    private void drawHelp(Graphics2D g2d) {
        int helpHeight = (getHeight() - getWidth()) / 2;
        int helpWidth = getWidth();
        
        int arrowSize = helpHeight / 4;
        Rectangle2D arrow = ShapeUtils.getRectangle(
                arrowSize,
                arrowSize,
                ViewPort.OriginPosition.eCentered);
        
        AffineTransform af = new AffineTransform();
        
        // up arrow
        af.setToTranslation(0, -arrowSize);
        af.translate(getWidth() / 2, getHeight() - helpHeight / 2);
        g2d.setPaint(Utils.getColor(Utils.buttonToValue(ArcadeButton.eUP)));
        g2d.fill(af.createTransformedShape(arrow));
        
        // right arrow
        af.setToTranslation(arrowSize, 0);
        af.translate(getWidth() / 2, getHeight() - helpHeight / 2);
        g2d.setPaint(Utils.getColor(Utils.buttonToValue(ArcadeButton.eRIGHT)));
        g2d.fill(af.createTransformedShape(arrow));
        
        // down arrow
        af.setToTranslation(0, arrowSize);
        af.translate(getWidth() / 2, getHeight() - helpHeight / 2);
        g2d.setPaint(Utils.getColor(Utils.buttonToValue(ArcadeButton.eDOWN)));
        g2d.fill(af.createTransformedShape(arrow));
        
        // left arrow
        af.setToTranslation(-arrowSize, 0);
        af.translate(getWidth() / 2, getHeight() - helpHeight / 2);
        g2d.setPaint(Utils.getColor(Utils.buttonToValue(ArcadeButton.eLEFT)));
        g2d.fill(af.createTransformedShape(arrow));
        
        // button 1
        af.setToTranslation(arrowSize * 3, 0);
        af.translate(getWidth() / 2, getHeight() - helpHeight / 2);
        g2d.setPaint(Utils.getColor(Utils.buttonToValue(ArcadeButton.eBTN1)));
        g2d.fill(af.createTransformedShape(arrow));
    }
    
    private void drawDot(Graphics2D g2d, Grid g, double s, Cell cell, Color c) {
        if (dot != null) {
            AffineTransform af = new AffineTransform();
            Vector2D loc = getCellLocation(g, cell);
            int w = getCellSize(g);
            af.translate(loc.dX + w / 2, loc.dY + w / 2);
            Shape shape = af.createTransformedShape(dot);

            g2d.setPaint(c);
            g2d.fill(shape);
        }
    }
    
    private int getCellSize(Grid grid) {
        return getWidth() / grid.getSize();
    }
    
    private int getGridWidth(Grid grid) {
        int w = getCellSize(grid);
        return w * grid.getSize();
    }
    
    private Vector2D getCellLocation (Grid grid, Cell c) {
        Vector2D loc = new Vector2D();
        int w = getCellSize(grid);
        int gridWidth = getGridWidth(grid);
        
        loc.dX = (getWidth() - gridWidth) / 2;
        loc.dY = (getHeight() - gridWidth) / 2;
        loc = loc.add(new Vector2D(w * c.x, w * c.y));
        
        return loc;
    }
    
    private void drawBridge(Graphics2D g2d, Grid g, Cell c1, Cell c2) {
        if (bridge != null) {
            Color c = Utils.getColor(g.getValue(c1));
            Vector2D l1 = getCellLocation(g, c1);
            Vector2D l2 = getCellLocation(g, c2);
            Vector2D diff = l2.sub(l1);
            diff = diff.scale(0.5);

            int w = getCellSize(g);
            AffineTransform af = new AffineTransform();
            af.translate(l1.dX, l1.dY);
            af.translate(diff.dX, diff.dY);
            Shape b = af.createTransformedShape(bridge);

            g2d.setPaint(c);
            g2d.fill(b);
        }
    }

    @Override
    public void draw(Graphics2D g2d) {
    }

    @Override
    public void onResize(ResizeEvent re) {
        if (grid != null) {
            int s = getCellSize(grid);
            dot = ShapeUtils.getEllipse(s,s,ViewPort.OriginPosition.eCentered);
            bridge = ShapeUtils.getRectangle(s,s,ViewPort.OriginPosition.eUpperLeft);
        }
    }

    @Override
    public void onPause() {
        sw.logSummary();
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onBoot() {
    }

    @Override
    public void onShutdown() {
    }
    
}
