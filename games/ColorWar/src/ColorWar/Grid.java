/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ColorWar;

import java.util.Random;

/**
 *
 * @author daniel
 */
public class Grid {
    private final Random rand;
    private final int[][] grid;
    private final int size;
    private final int n;
    
    /**
     * @param size
     * The size of the grid
     * 
     * @param n
     * The number of values each cell can contain
     */
    public Grid(int size, int n) {
        rand = new Random(0);
        grid = new int[size][size];
        this.size = size;
        this.n = n;
        
        randomize();
    }
    
    public void randomize() {
        for (int i=0; i<size*size; i++)
        {
            grid[i%size][i/size] = rand.nextInt(n);
        }
    }
    
    public int getSize() {
        return size;
    }
    
    public int getValue(Cell c) {
        return getValue(c.x, c.y);
    }
    
    public int getValue(int x, int y) {
        checkBounds(x, y);
        return grid[x][y];
    }
    
    public void setValue(Cell c, int v) {
        setValue(c.x, c.y, v);
    }
    
    public void setValue(int x, int y, int v) {
        checkBounds(x, y);
        grid[x][y] = v;
    }
    
    public boolean inBounds(Cell c) {
        return inBounds(c.x, c.y);
    }
    
    public boolean inBounds(int x, int y) {
        try {
            checkBounds(x, y);
        } catch (IllegalArgumentException ex) {
            return false;
        }
        return true;
    }
    
    private void checkBounds(int x, int y) {
        if (x < 0 || x >= size) {
            throw new IllegalArgumentException(
                    String.format("x is out of grid bounds. x = %d", x));
        } else if (y < 0 || y >= size) {
            throw new IllegalArgumentException(
                    String.format("y is out of grid bounds. y = %d", y));
        }
    }
}
