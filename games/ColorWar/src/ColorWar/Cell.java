/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ColorWar;

/**
 *
 * @author daniel
 */
public class Cell implements Comparable<Cell>{
    int x,y;

    private Cell(){
    }

    public Cell(int x, int y) {
        this.x = x;
        this.y = y; 
    }

    @Override
    public int compareTo(Cell o) {
        if (x < o.x) {
            return -1;
        } else if (x == o.x) {
            if (y < o.y) {
                return -1;
            } else if (y == o.y) {
                return 0;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return String.format("(%d,%d)", x,y);
    }
    
    
}
