/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ColorWar;

import Game.Audio.AudioClipStore;
import Game.Data.PlayerControls;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;

/**
 *
 * @author daniel
 */
public class Player implements KeyListener{
    Grid grid;
    Set<Cell> captures;
    boolean keyPressed;
    PlayerControls pc;

    public Player(Grid g, Cell seedCell) {
        grid = g;
        captures = new TreeSet<>();
        keyPressed = false;
        
        captures.add(seedCell);
        flood(grid.getValue(seedCell));
    }
    
    public void init(PlayerControls pc) {
        this.pc = pc;
    }
    
    public Set<Cell> getCaptures() {
        return new TreeSet<>(captures);
    }
    
    public void flood(int v) {
        for (Cell c : captures) {
            grid.setValue(c, v);
        }
        
        Iterator<Cell> root = captures.iterator();
        SortedSet<Cell> newCaptures = new TreeSet<>();
        explore(grid, newCaptures, root.next(), v);
        captures.addAll(newCaptures);
    }
    
    public void explore(Grid grid, SortedSet<Cell> capturedCells, Cell r, int voi) {
        int s = grid.getSize();
        SortedSet<Cell> q = new TreeSet<>();
        q.add(r);
        
        while (!q.isEmpty()) {
            Cell c = q.first();
            q.remove(c);
            capturedCells.add(c);
            
            /**
             * Explore cells around c. If they have the same color, then add
             * them to the queue they will be capture the next iteration of the
             * while loop.
             */
            Cell t = new Cell(c.x, c.y);
            t.x = c.x + 1;
            t.y = c.y;
            if (grid.inBounds(t) && !capturedCells.contains(t) && grid.getValue(t) == voi) {
                // right
                q.add(new Cell(t.x,t.y));
            }
            
            t.x = c.x - 1;
            t.y = c.y;
            if (grid.inBounds(t) && !capturedCells.contains(t) && grid.getValue(t) == voi) {
                // left
                q.add(new Cell(t.x,t.y));
            }
            
            t.x = c.x;
            t.y = c.y + 1;
            if (grid.inBounds(t) && !capturedCells.contains(t) && grid.getValue(t) == voi) {
                // down
                q.add(new Cell(t.x,t.y));
            }
            
            t.x = c.x;
            t.y = c.y - 1;
            if (grid.inBounds(t) && !capturedCells.contains(t) && grid.getValue(t) == voi) {
                // down
                q.add(new Cell(t.x,t.y));
            }
        }
    }

    private void playFloodSound() {
        Clip flood = AudioClipStore.getClip("flood");
        flood.setFramePosition(0);
        flood.start();
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!keyPressed && pc != null) {
            flood(Utils.keyCodeToValue(e.getKeyCode(), pc));
            playFloodSound();
        }
        keyPressed = true;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        keyPressed = false;
    }
}
