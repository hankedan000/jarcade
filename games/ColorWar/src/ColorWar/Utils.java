/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ColorWar;

import Game.Data.PlayerControls;
import Game.Data.PlayerControls.ArcadeButton;
import java.awt.Color;

/**
 *
 * @author daniel
 */
public class Utils {
    static Color[] colors = {
        new Color(0xff8585),
        new Color(0x69a3ff),
        new Color(0x00ecab),
        new Color(0xffb579),
        new Color(0xd2a4ff)
    };
    
    public static Color getColor(int i) {
        return colors[i];
    }
    public static int keyCodeToValue(int keyCode, PlayerControls pc) {
        if (keyCode == pc.UP) {
            return buttonToValue(ArcadeButton.eUP);
        } else if (keyCode == pc.DOWN) {
            return buttonToValue(ArcadeButton.eDOWN);
        } else if (keyCode == pc.LEFT) {
            return buttonToValue(ArcadeButton.eLEFT);
        } else if (keyCode == pc.RIGHT) {
            return buttonToValue(ArcadeButton.eRIGHT);
        } else {
            return buttonToValue(ArcadeButton.eBTN1);
        }
    }
    
    public static int buttonToValue(ArcadeButton btn) {
        switch (btn) {
            case eUP:
                return 0;
            case eDOWN:
                return 1;
            case eLEFT:
                return 2;
            case eRIGHT:
                return 3;
            case eBTN1:
                return 4;
        }
        throw new IllegalArgumentException (
                btn.name() + "is not supported yet.");
    }
}
