
import ColorWar.Cell;
import ColorWar.GamePanel;
import ColorWar.Grid;
import ColorWar.Player;
import Game.Audio.AudioClipStore;
import Game.Data.GameData;
import Game.Data.GameInfo;
import Game.JGame;
import Game.Services.GameServices;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.sound.sampled.Clip;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author daniel
 */
public class GameMain extends JGame {
    Grid grid;
    GamePanel gp;
    Player p1;
    Player p2;

    public GameMain() {
        grid = new Grid(30, 5);
        gp = new GamePanel(this);
        p1 = new Player(grid, new Cell(grid.getSize() / 2, grid.getSize() - 1));
        p2 = new Player(grid, new Cell(grid.getSize() / 2, 0));
        gp.setGrid(grid);
        gp.addPlayer(p1);
        gp.addPlayer(p2);
        
        addGamePanel(gp);
    }
    
    @Override
    public GameInfo getGameInfo() {
        return new ColorWar.GameInfo();
    }
    
    @Override
    public void boot(GameServices gs) {
        super.boot(gs);
        
        // get the game data from the GameDataService
        GameData gd = gs.getGameDataService().load(getGameInfo());
        
        p1.init(gd.getPlayer1Controls());
        p2.init(gd.getPlayer2Controls());
        addKeyListener(p1);
        addKeyListener(p2);
        addKeyListener(new CollisionChecker());
        
        addDrawable(gp);
        addGameStateListener(gp);
        
        System.out.println("Loading audio clips...");
        ClassLoader cl = this.getClass().getClassLoader();
        AudioClipStore.load(cl,"wav/flood.wav","flood");
        AudioClipStore.load(cl,"wav/background.wav","background");
    }
    
    class CollisionChecker implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyPressed(KeyEvent e) {
            Set<Cell> intersection = new TreeSet<>(p1.getCaptures());
            intersection.retainAll(p2.getCaptures());
            
            if (intersection.size() > 0) {
                System.out.println("Game over");
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    @Override
    public void pause() {
        super.pause();
        
        Clip backgroundClip = AudioClipStore.getClip("background");
        backgroundClip.stop();
    }

    @Override
    public void resume() {
        super.resume();
        
        Clip backgroundClip = AudioClipStore.getClip("background");
//        backgroundClip.start();
    }
}
