
import SpaceAce.BulletSpawnInterface;
import Game.Data.GameData;
import Game.Data.GameInfo;
import Game.Draw.GamePanel;
import Game.Draw.PanelPlacement;
import Game.Draw.ResizeEvent;
import Game.JGame;
import Game.Physics.CollisionGroup;
import Game.Primitives.Vector2D;
import Game.Services.GameServices;
import SpaceAce.Alien;
import SpaceAce.Ship;
import SpaceAce.SmallBullet;
import SpaceAce.Space;
import SpaceAce.SpaceAceGameInfo;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author daniel
 */
public class GameMain extends JGame
                      implements BulletSpawnInterface{
    Space space;
    Ship ship;
    CollisionGroup colGroup;

    public GameMain() {
        space = new Space(getRenderer());
        addGamePanel(space);// add the panel so that it's redrawn every frame
        
        // Create our ship
        ship = new Ship(this);
        addObject(ship);
        
        Alien alien = new Alien();
        addObject(alien);
        colGroup = getCollisionEngine().getCollisionGroup("ALIEN_BULLET");
        colGroup.add(alien);
    }

    @Override
    public void boot(GameServices gs) {
        super.boot(gs);
        
        // get the game data from the GameDataService
        GameData gd = gs.getGameDataService().load(getGameInfo());
        
        // Initialize the ship with controls, and then add it's key listener
        ship.init(gd.getPlayer1Controls());
        addKeyListener(ship.getKeyListener());
    }
    
    @Override
    public GameInfo getGameInfo() {
        return new SpaceAceGameInfo();
    }

    @Override
    public List<GamePanel> getGamePanels() {
        List<GamePanel> panels = new ArrayList<>();
        panels.add(space);
        return panels;
    }

    @Override
    public void spawnSmallBullet(Vector2D p) {
        SmallBullet b = new SmallBullet();
        b.setPosition(p);
        
        // Make a resize event so that the bullet can size itself correctly
        // according to the screen
        Dimension dims[] = new Dimension[1];
        dims[0] = space.getSize();
        ResizeEvent re = new ResizeEvent(PanelPlacement.eSingle, dims);
        
        // Allow the bullet to scale its self properly
        b.onResize(re);
        
        addObject(b);
        colGroup.add(b);
    }
}
