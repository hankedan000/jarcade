/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpaceAce;

import Game.Draw.ResizeEvent;
import Game.Draw.ViewPort;
import Game.Primitives.GameObject;
import Game.Utilities.ShapeUtils;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author daniel
 */
public class SmallBullet extends GameObject{
    private final double BULLET_W_TO_BULLET_H = 1.0 / 1.0;
    private final double SCREEN_W_TO_BULLET_W = 1.0 / 70.0;
    private Rectangle2D body;
    private int topOfScreen;
    
    public SmallBullet() {
        setStatic(false);
        
        // bullets can collide with Alien objects
        setCollidable(true);
        setCollidableID(Constants.ALIEN_BULLET_COLLISION_ID);
    }
    
    @Override
    protected Area getLocalBounds() {
        return new Area(body);
    }

    @Override
    public void draw(Graphics2D g2d) {
        if (body != null)
        {
            AffineTransform at = AffineTransform.getTranslateInstance(s_.dX,
                                                                      s_.dY);
            Shape transformBody = at.createTransformedShape(body);
            g2d.setPaint(Constants.BULLET_COLOR);
            g2d.fill(transformBody);
        }
    }

    @Override
    public void onResize(ResizeEvent re) {
        int spaceW = re.dimensions[0].width;
        int spaceH = re.dimensions[0].height;
        
        // make the bullet body
        int bulletW = (int)(spaceW * SCREEN_W_TO_BULLET_W);
        int bulletH = (int)(bulletW * BULLET_W_TO_BULLET_H);
        body = ShapeUtils.getRectangle(bulletW,
                                       bulletH,
                                       ViewPort.OriginPosition.eCentered);
        
        // the bullet should take 0.5 seconds to move up the screen
        v_.dY = spaceH / -0.5;
        
        // used to kill the bullet when it leaves the screen
        topOfScreen = spaceH / 2 * -1;
    }

    @Override
    public void onSimComplete() {
        if (s_.dY < topOfScreen)
        {
            kill();
        }
    }
    
}
