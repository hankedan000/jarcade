package SpaceAce;


import Game.Primitives.Vector2D;
import SpaceAce.SmallBullet;

/**
 *
 * @author daniel
 */
public interface BulletSpawnInterface {
    void spawnSmallBullet(Vector2D p);
}
