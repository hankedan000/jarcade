/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpaceAce;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 *
 * @author daniel
 */
public class ShipControlListener implements KeyListener {
    private final OnShipControl osc;
    private final int leftKey, rightKey, shootKey;
    private boolean leftPressed;
    private boolean rightPressed;
    private boolean shootPressed;
    
    public ShipControlListener(
            OnShipControl onShipControl,
            int leftKeyCode,
            int rightKeyCode,
            int shootKeyCode) {
        osc = onShipControl;
        leftKey = leftKeyCode;
        rightKey = rightKeyCode;
        shootKey = shootKeyCode;
        
        // initialize pressed states to false
        leftPressed = false;
        rightPressed = false;
        shootPressed = false;
    }
    
    @Override
    public void keyTyped(KeyEvent ke) {
        // do nothing
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == leftKey && !leftPressed) {
            osc.onLeft();
            leftPressed = true;
            
            /**
             * A joystick on an arcade machine can only be in move position at
             * a time. To simulate this when using a keyboard, we must force
             * the right key to be released when the left is pressed.
             */
            rightPressed = false;
        } else if(ke.getKeyCode() == rightKey && !rightPressed) {
            osc.onRight();
            rightPressed = true;
            leftPressed = false;// see above comment on joystick simulation
        } else if (ke.getKeyCode() == shootKey && !shootPressed) {
            osc.onShoot();
            shootPressed = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        boolean checkForStop = false;
        
        if (ke.getKeyCode() == leftKey) {
            leftPressed = false;
            checkForStop = true;
        } else if(ke.getKeyCode() == rightKey) {
            rightPressed = false;
            checkForStop = true;
        } else if (ke.getKeyCode() == shootKey) {
            shootPressed = false;
        }
        
        // Are both movement keys are released?
        if (checkForStop && !leftPressed && !rightPressed) {
            // if so, notify ship to stop moving
            osc.onStop();
        }
    }
    
    public interface OnShipControl
    {
        void onLeft();
        void onRight();
        void onStop();
        void onShoot();
    }
    
}
