/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpaceAce;

import Game.Draw.ResizeEvent;
import Game.Draw.ViewPort;
import Game.Physics.CollisionEvent;
import Game.Primitives.GameObject;
import Game.Utilities.ShapeUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author daniel
 */
public class Alien extends GameObject{
    private final double ALIEN_W_TO_ALIEN_H  = 4.0 / 3.0;
    private final double SCREEN_W_TO_ALIEN_W = 1.0 / 20.0;
    private Rectangle2D body;
    private Point ip, fp;

    public Alien() {
        // Alien's can collides with bullets
        setCollidable(true);
        setCollidableID(Constants.ALIEN_BULLET_COLLISION_ID);
    }
    
    @Override
    protected Area getLocalBounds() {
        return new Area(body);
    }

    @Override
    public void draw(Graphics2D g2d) {
        if (body != null)
        {
            AffineTransform at = AffineTransform.getTranslateInstance(s_.dX,
                                                                      s_.dY);
            Shape transformBody = at.createTransformedShape(body);
            g2d.setPaint(Constants.ALIEN_COLOR);
            g2d.fill(transformBody);
        }
        
        drawPoints(g2d);
        drawPath(g2d);
    }
    
    private void drawPoints(Graphics2D g2d) {
        final int DOT_SIZE = 5;
        Shape dot = ShapeUtils.getEllipse(DOT_SIZE,
                                          DOT_SIZE,
                                          ViewPort.OriginPosition.eCentered);
        if (ip != null) {
            AffineTransform at = AffineTransform.getTranslateInstance(ip.x,
                                                                      ip.y);
            
            g2d.setPaint(Color.GREEN);
            g2d.fill(at.createTransformedShape(dot));
        }
        if (fp != null) {
            AffineTransform at = AffineTransform.getTranslateInstance(fp.x,
                                                                      fp.y);
            
            g2d.setPaint(Color.RED);
            g2d.fill(at.createTransformedShape(dot));
        }
    }
    
    private void drawPath(Graphics2D g2d) {
        if (ip != null) {
            Path2D path = new Path2D.Double();
            path.moveTo(ip.x, ip.y);
            PointerInfo a = MouseInfo.getPointerInfo();
            Point b = a.getLocation();
            path.curveTo(10,10,b.x,b.y,fp.x, fp.y);
            
            g2d.setPaint(Color.CYAN);
            g2d.draw(path);
        }
    }

    @Override
    public void onResize(ResizeEvent re) {
        int spaceW = re.dimensions[0].width;
        int spaceH = re.dimensions[0].height;
        
        ip = new Point(-spaceW / 2, 0);
        fp = new Point(0, -spaceH / 4);
        
        // make the alein body
        int alienW = (int)(spaceW * SCREEN_W_TO_ALIEN_W);
        int alienH = (int)(alienW * ALIEN_W_TO_ALIEN_H);
        body = ShapeUtils.getRectangle(alienW,
                                       alienH,
                                       ViewPort.OriginPosition.eCentered);
        
        s_.dY = 0 - spaceH / 4;
        s_.dX = 0;
    }
    
    @Override
    protected void onCollisionEvent(CollisionEvent ce) {
        kill();
    }
}
