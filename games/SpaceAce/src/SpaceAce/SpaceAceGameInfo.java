/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpaceAce;

import Game.Data.GameInfo;
import Game.Draw.PanelPlacement;
import java.awt.Color;

/**
 *
 * @author daniel
 */
public class SpaceAceGameInfo implements GameInfo{

    @Override
    public String getName() {
        return "SpaceAce";
    }

    @Override
    public PanelPlacement getPanelPlacement() {
        return PanelPlacement.eSingle;
    }

    @Override
    public double getAspectRatio() {
        return 300.0 / 400.0;
    }

    @Override
    public Color getBezelColor() {
        return Constants.BEZEL_COLOR;
    }
    
    @Override
    public int minPlayers() {
        return 1;
    }

    @Override
    public int maxPlayers() {
        return 2;
    }
    
}
