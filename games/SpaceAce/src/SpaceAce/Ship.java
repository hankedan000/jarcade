/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpaceAce;

import Game.Data.PlayerControls;
import Game.Draw.ResizeEvent;
import Game.Draw.ViewPort;
import Game.Primitives.GameObject;
import Game.Primitives.Vector2D;
import Game.Utilities.ShapeUtils;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author daniel
 */
public class Ship extends GameObject
                  implements ShipControlListener.OnShipControl{
    private final double SHIP_W_TO_SHIP_H = 4.0 / 3.0;
    private final double SCREEN_W_TO_SHIP_W = 1.0 / 20.0;
    private double maxMoveVelocity;
    private int maxLeftPos, maxRightPos;
    private Rectangle2D body;
    private boolean isInitialResize;
    private ShipControlListener scl;
    private final BulletSpawnInterface bsi;
    
    public Ship(BulletSpawnInterface bulletSpawnInterface)
    {
        super();
        isInitialResize = true;
        bsi = bulletSpawnInterface;
        
        // so that the ship can actually physically move
        setStatic(false);
    }
    
    public void init(PlayerControls pc) {
        scl = new ShipControlListener(this, pc.LEFT, pc.RIGHT, pc.BTN1);
    }
    
    public KeyListener getKeyListener() {
        return scl;
    }
    
    @Override
    protected Area getLocalBounds() {
        return new Area(body);
    }

    @Override
    public void draw(Graphics2D g2d) {
        if (body != null)
        {
            AffineTransform at = AffineTransform.getTranslateInstance(s_.dX,
                                                                      s_.dY);
            Shape transformBody = at.createTransformedShape(body);
            g2d.setPaint(Constants.SHIP_COLOR);
            g2d.fill(transformBody);
        }
    }

    @Override
    public void onResize(ResizeEvent re) {
        int spaceW = re.dimensions[0].width;
        int spaceH = re.dimensions[0].height;
        
        // make the ship body
        int shipW = (int)(spaceW * SCREEN_W_TO_SHIP_W);
        int shipH = (int)(shipW * SHIP_W_TO_SHIP_H);
        body = ShapeUtils.getRectangle(shipW,
                                       shipH,
                                       ViewPort.OriginPosition.eCentered);
        
        // ship will alway be be half a ship's height from bottom of screen
        s_.dY = spaceH / 2.0 - shipH;
        if (isInitialResize)
        {
            s_.dX = 0;
        }
        
        // the ship should take 3 seconds to move clear across the screen
        maxMoveVelocity = spaceW / 3.0;
        
        // compute the ships maximum bounds
        maxRightPos = spaceW / 2 - shipW / 2;
        maxLeftPos = -maxRightPos;
        
        isInitialResize = false;
    }

    @Override
    public void onLeft() {
        System.out.println("Moving left");
        v_.dX = -maxMoveVelocity;
    }

    @Override
    public void onRight() {
        System.out.println("Moving right");
        v_.dX = maxMoveVelocity;
    }

    @Override
    public void onStop() {
        System.out.println("Stopped");
        v_.dX = 0;
    }

    @Override
    public void onShoot() {
        spawnSingleShot();
    }
    
    private void spawnSingleShot() {
        bsi.spawnSmallBullet(s_);
    }
    
    private void spawnTripleShot() {
        double hOffset = body.getWidth();
        double vOffset = body.getHeight() / 2;
        bsi.spawnSmallBullet(s_);
        bsi.spawnSmallBullet(s_.add(new Vector2D(hOffset, vOffset)));
        bsi.spawnSmallBullet(s_.add(new Vector2D(-hOffset, vOffset)));
    }

    @Override
    public void onSimComplete() {
        if (s_.dX < maxLeftPos)
        {
            s_.dX = maxLeftPos;
            onStop();;
        } else if (s_.dX > maxRightPos) {
            s_.dX = maxRightPos;
            onStop();
        }
    }
    
}

