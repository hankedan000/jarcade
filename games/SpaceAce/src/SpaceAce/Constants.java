package SpaceAce;

import java.awt.Color;

/**
 * This class contains constants for SpaceAce
 */
public class Constants {
    // restrict instantiation
    private Constants(){}
    
    // Collision groups
    public static final long ALIEN_BULLET_COLLISION_ID = 0;
    public static final long ALIEN_SHIP_COLLISION_ID   = 1;
    
    // Colors
    public static final Color SPACE_COLOR  = Color.BLACK;
    public static final Color BEZEL_COLOR  = Color.DARK_GRAY;
    public static final Color SHIP_COLOR   = Color.WHITE;
    public static final Color BULLET_COLOR = Color.RED;
    public static final Color ALIEN_COLOR  = Color.GREEN;
}
