/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SpaceAce;

import Game.Draw.GamePanel;
import Game.Draw.RendererInterface;
import Game.Draw.ViewPort;
import Game.Utilities.ShapeUtils;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author daniel
 */
public class Space extends GamePanel{
    
    public Space(RendererInterface renderer) {
        super(renderer);
        
        viewport.setOriginPosition(ViewPort.OriginPosition.eCentered);
        setBackground(Constants.SPACE_COLOR);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
    }
    
    private void drawOrigin(Graphics2D g2d) {
        // Draw crosshair where (0,0) is
        g2d.setColor(Color.LIGHT_GRAY);
        g2d.drawLine(-100, 0, 100, 0);
        g2d.drawLine(0, 100, 0, -100);
        
        Polygon arrow = new Polygon();
        arrow.addPoint(4, 100);
        arrow.addPoint(-4, 100);
        arrow.addPoint(0, 105);
        g2d.setColor(Color.YELLOW);
        g2d.fill(arrow);
    }
    
    private void drawPerimeter(Graphics2D g2d) {
        Rectangle2D box = ShapeUtils.getRectangle(
                getWidth() - 10,
                getHeight() - 10,
                ViewPort.OriginPosition.eCentered);
        
        g2d.setColor(Color.BLUE);
        g2d.draw(box);
    }
    
}

