/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Draw;

import Game.Physics.Collidable;
import Game.Physics.CollisionEvent;
import Game.Primitives.Vector2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import javax.swing.JPanel;

/**
 *
 * @author daniel
 */
public class ViewPort extends Collidable
{
    public enum OriginPosition
    {
        eCentered, eUpperLeft
    };
    
    private String name;
    private Vector2D origin = null;
    private JPanel panel = null;
    private OriginPosition originPosition = OriginPosition.eCentered;
    private double rotateAngle = 0;
    private boolean vFlip = false;
    
    private
    ViewPort()
    {
    }
    
    public void
    setRotateAngle(
            double radians)
    {
        rotateAngle = radians;
    }
    
    public void
    setVerticalFlipped(
            boolean flipped)
    {
        vFlip = flipped;
    }
    
    public
    ViewPort(
            JPanel panel)
    {
        this(panel,"");
        origin = new Vector2D();
        this.panel = panel;
    }
    
    public
    ViewPort(
            JPanel panel,
            String name)
    {
        origin = new Vector2D();
        this.panel = panel;
        this.name = name;
    }
    
    public void
    setOriginPosition(
            OriginPosition op)
    {
        originPosition = op;
    }
    
    public Vector2D
    getOrigin()
    {
        return origin;
    }
    
    public void
    setOrigin(
            Vector2D newOrigin)
    {
        origin = newOrigin;
    }
    
    public AffineTransform
    getLocalTransform()
    {
        double halfScreenW = panel.getWidth() / 2;
        double halfScreenH = panel.getHeight() / 2;
        
        AffineTransform trans = new AffineTransform();
        trans.rotate(rotateAngle);
        
        AffineTransform recenterTrans = new AffineTransform();
        recenterTrans.rotate(rotateAngle);
        switch(originPosition)
        {
            case eUpperLeft:
                // it's already in the upper left, so do nothing
                break;
            case eCentered:
                recenterTrans.translate(halfScreenW, halfScreenH);
                break;
        }
        
        // Move the viewport origin to the location in the world
        trans.translate(recenterTrans.getTranslateX(),
                        recenterTrans.getTranslateY());
        
        return trans;
    }
    
    public AffineTransform
    getGlobalTransform()
    {
        AffineTransform trans = getLocalTransform();
        
        trans.translate(-origin.dX, -origin.dY);
        
        return trans;
    }
    
    @Override
    public Area getGlobalBounds() {
        Rectangle.Double rect = new Rectangle.Double();
        
        switch(originPosition)
        {
            case eCentered:
                rect = new Rectangle.Double(
                        origin.dX - panel.getWidth() / 2,
                        origin.dY - panel.getHeight() / 2,
                        panel.getWidth(),
                        panel.getHeight());
                break;
            case eUpperLeft:
                rect = new Rectangle.Double(
                        origin.dX,
                        origin.dY,
                        panel.getWidth(),
                        panel.getHeight());
                break;
        }
        
        return new Area(rect);
    }
    
    @Override
    protected void onCollisionEvent(CollisionEvent ce)
    {
        // do nothing
    }

    @Override
    protected void onCollisionEnter(CollisionEvent ce) {
        // do nothing
    }

    @Override
    protected void onCollisionExit(CollisionEvent ce) {
    }
}
