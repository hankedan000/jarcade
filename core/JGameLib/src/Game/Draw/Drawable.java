/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Draw;

import java.awt.Graphics2D;

/**
 *
 * @author daniel
 */
public interface Drawable
{
    /**
     * Description:
     * Called when the object is requested to draw itself
     * 
     * @param g2d 
     * The Graphics2D that the object should draw itself within.
     */
    public void
    draw(
            Graphics2D g2d);
    
    /**
     * Description:
     * This method is called when any of the GamePanel's are resized.
     * 
     * @param re
     * Event containing the new dimensions for each of the GamePanel's. The
     * ordering in the array is the same ordering in which the panels were
     * added to the Game.
     */
    public void
    onResize(
            ResizeEvent re);
}
