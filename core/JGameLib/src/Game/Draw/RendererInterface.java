/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Draw;

import java.util.Collection;
import java.util.List;

/**
 *
 * @author daniel
 */
public interface RendererInterface
{
    public int
    getFramesPerSecond();
        
    public void
    addDrawable(
            Drawable drawable);
    
    public void
    removeDrawable(
            Drawable drawable);
    
    public void
    addDrawables(
            Collection<Drawable> drawables);
    
    public List<Drawable>
    getDrawables();
    
    public void
    notifyResize(
            ResizeEvent re);
}
