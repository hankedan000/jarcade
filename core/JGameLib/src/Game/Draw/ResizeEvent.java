/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Draw;

import java.awt.Dimension;

/**
 *
 * @author daniel
 */
public class ResizeEvent
{
    public PanelPlacement placement;
    public Dimension[] dimensions;
    
    public ResizeEvent(
            PanelPlacement panelPlacement,
            Dimension[] panelDimensions)
    {
        placement  = panelPlacement;
        dimensions = panelDimensions;
    }
}
