/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Audio;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.classpath.icedtea.pulseaudio.PulseAudioClip;

/**
 *
 * @author daniel
 */
public class AudioClipStore {
    private static AudioClipStore instance;
    private Map<String,Clip> clips;
    private Clip emptyClip;
    
    private AudioClipStore() {
        clips = new HashMap<>();
        try {
            emptyClip = AudioSystem.getClip();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioClipStore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static AudioClipStore getInstance() {
        if (instance == null) {
            instance = new AudioClipStore();
        }
        return instance;
    }
    
    public static void load(ClassLoader cl, String audioResource, String clipName) {
        try {
            AudioInputStream stream;
            AudioFormat format;
            DataLine.Info info;
            Clip clip;
            
            InputStream is = cl.getResourceAsStream(audioResource);
            stream = AudioSystem.getAudioInputStream(is);
            format = stream.getFormat();
            info = new DataLine.Info(Clip.class, format);
            clip = (Clip) AudioSystem.getLine(info);
            clip.open(stream);
            
            // store for later
            getInstance().clips.put(clipName, clip);
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(AudioClipStore.class.getName()).log(
                    Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AudioClipStore.class.getName()).log(
                    Level.SEVERE, null, ex);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioClipStore.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }
    
    public static Clip getClip(String clipName) {
        AudioClipStore acs = getInstance();
        if (!acs.clips.containsKey(clipName)) {
            acs.getLogger().log(Level.SEVERE,
                    String.format("\"%s\" isn't loaded!", clipName));
            return acs.emptyClip;
        }
        return acs.clips.get(clipName);
    }
    
    private Logger getLogger() {
        return Logger.getLogger(this.getClass().getName());
    }
}
