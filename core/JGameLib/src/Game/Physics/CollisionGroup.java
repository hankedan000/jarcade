/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author daniel
 */
public class CollisionGroup {

    private final String groupName;
    private HashMap<Long, Collidable> collidables;
    private HashMap<Long, List<Collidable>> collisionHistory;
    private boolean onlyNoneSimilar = false;

    private CollisionGroup() {
        groupName = "INVALID";
    }

    public CollisionGroup(String name) {
        groupName = name;
        collidables = new HashMap<>();
        collisionHistory = new HashMap<>();
    }

    public void setOnlyCollideNoneSimilar(boolean onlyNoneSimilar) {
        this.onlyNoneSimilar = onlyNoneSimilar;
    }

    public String getName() {
        return groupName;
    }

    public void add(Collidable collidable) {
        collidables.put(collidable.UUID, collidable);
        collisionHistory.put(collidable.UUID,
                new ArrayList<Collidable>());
    }

    public void addAll(Collection<Collidable> collidables) {
        for (Collidable coll : collidables) {
            add(coll);
        }
    }
    
    void removeCollidable(Collidable c) {
        collidables.remove(c.UUID);
        
        // make sure we remove it from any collision histories
        List<Collidable> history = collisionHistory.remove(c.UUID);
        if (history != null) {
            for (Collidable other : history) {
                removeFromHistory(other, c);
            }
        }
    }

    public int getCollidableCount() {
        return collidables.size();
    }

    public void checkAndNotify() {
        CollisionEvent ce = new CollisionEvent(groupName, null, null);
        ArrayList<Collidable> list = new ArrayList(collidables.values());

        for (int a = 0; a < list.size(); a++) {
            Collidable collA = list.get(a);

            if (!collA.isCollidable()) {
                continue;
            }

            for (int b = a + 1; b < list.size(); b++) {
                Collidable collB = list.get(b);

                if (!collB.isCollidable()) {
                    continue;
                }

                if (onlyNoneSimilar
                        && collA.getCollidableID() == collB.getCollidableID()) {
                    // a and b are of the same collidable ID so skip
                    continue;
                }

                ce.area = collA.getGlobalBounds();
                ce.area.intersect(collB.getGlobalBounds());

                if (!ce.area.isEmpty()) {
                    ce.other = collB;
                    collA.onCollisionEvent(ce);
                    if (!wasPreviouslyCollided(collA, collB)) {
                        collA.onCollisionEnter(ce);
                        addToHistory(collA, collB);
                    }

                    ce.other = collA;
                    collB.onCollisionEvent(ce);
                    if (!wasPreviouslyCollided(collB, collA)) {
                        collB.onCollisionEnter(ce);
                        addToHistory(collB, collA);
                    }
                } else {
                    if (wasPreviouslyCollided(collA, collB)) {
                        collA.onCollisionExit(ce);
                        removeFromHistory(collA, collB);
                    }

                    if (wasPreviouslyCollided(collB, collA)) {
                        collB.onCollisionExit(ce);
                        removeFromHistory(collB, collA);
                    }
                }
            }
        }
    }

    private void addToHistory(Collidable a, Collidable b) {
        if (a != b) {
            collisionHistory.get(a.UUID).add(b);
        }
    }

    private void removeFromHistory(Collidable a, Collidable b) {
        if (a != b) {
            collisionHistory.get(a.UUID).remove(b);
        }
    }

    private boolean wasPreviouslyCollided(Collidable a, Collidable b) {
        return collisionHistory.get(a.UUID).contains(b);
    }
}
