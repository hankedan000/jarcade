/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

import Game.GameStateListener;
import Game.Primitives.Vector2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;
import javax.swing.Timer;

/**
 *
 * @author daniel
 */
public class PhysicsEngine implements ActionListener,
                                      GameStateListener
{
    Timer timer;
    List<PhysicsEntity2D> entities;
    Stack<OnSimListener> onSimListeners;
    double dtMillis;
    Vector2D gravity;
    Stack<PhysicsEntity2D> markedForRemoval;
    
    public
    PhysicsEngine(
            int maxFPS)
    {
        timer = new Timer(1000/maxFPS, this);
        entities = new ArrayList<>();
        onSimListeners = new Stack<>();
        dtMillis = 1000.0/maxFPS;
        gravity = new Vector2D();
        markedForRemoval = new Stack<>();
    }
    
    public void
    setGravity(
            Vector2D g)
    {
        this.gravity = g;
        for(PhysicsEntity2D entity : entities)
        {
            entity.a_ = gravity;
        }
    }
    
    public void
    addEntity(
            PhysicsEntity2D entity)
    {
        entity.a_ = gravity;
        registerOnSimListener(entity);
        entities.add(entity);
    }
    
    public int getEntityCount() {
        return entities.size();
    }
    
    public final void
    removeEntity(
            PhysicsEntity2D entity)
    {
        // Add the entity to the stack of item to garbage to collect.
        // It will be removed after the simulation cycle has completed.
        markedForRemoval.push(entity);
    }
    
    public void
    addEntities(
            Collection<PhysicsEntity2D> entities)
    {
        for(PhysicsEntity2D entity : entities)
        {
            entity.a_ = gravity;
            this.entities.add(entity);
        }
    }
    
    public void
    registerOnSimListener(
            OnSimListener listener)
    {
        if(listener != null)
        {
            onSimListeners.push(listener);
        }
    }
    
    public List<OnSimListener> getOnSimListeners() {
        return onSimListeners;
    }
    
    public void
    unregisterOnSimListener(
            OnSimListener listener)
    {
        if(listener != null)
        {
            onSimListeners.remove(listener);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent ae)
    {
        notifySimBegin();
        
        for(PhysicsEntity2D entity : entities)
        {
            entity.sim(dtMillis);
        }
        
        notifySimComplete();
        
        // run a garbage collection if any entities were removed
        garbageCollectEntities();
    }
    
    private void
    notifySimBegin()
    {
        // TODO iterator better without copy
        Stack<OnSimListener> temp = new Stack<>();
        temp.addAll(onSimListeners);
        while(!temp.empty())
        {
            temp.pop().onSimBegin();
        }
    }
    
    private void
    notifySimComplete()
    {
        // TODO iterator better without copy
        Stack<OnSimListener> temp = new Stack<>();
        temp.addAll(onSimListeners);
        while(!temp.empty())
        {
            temp.pop().onSimComplete();
        }
    }
    
    /**
     * This method is called at the end of a simulation cycle.
     * It is critical that that this method has exclusive access to the entities
     * list in order to prevent concurrent modification.
     */
    private void garbageCollectEntities() {
        while (!markedForRemoval.empty()) {
            PhysicsEntity2D e = markedForRemoval.pop();
            unregisterOnSimListener(e);
            entities.remove(e);
        }
    }
    
    @Override
    public void
    onPause()
    {
        timer.stop();
    }

    @Override
    public void
    onResume()
    {
        timer.start();
    }

    @Override
    public void
    onBoot()
    {
        timer.start();
    }

    @Override
    public void
    onShutdown()
    {
        timer.stop();
    }
}
