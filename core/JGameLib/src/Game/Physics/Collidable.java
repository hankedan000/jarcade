/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

/**
 *
 * @author daniel
 */
public abstract class Collidable implements Bounded
{
    private static long nextUUID = 0;
    long UUID;
    long collidableID;
    boolean collisionEnabled;
    
    public Collidable()
    {
        UUID = nextUUID;
        collidableID = 0;
        nextUUID++;
        collisionEnabled = false;
    }
    
    public final void
    setCollidableID(
            long ID)
    {
        collidableID = ID;
    }
    
    public final long
    getCollidableID()
    {
        return collidableID;
    }
    
    public final void
    setCollidable(boolean collidable)
    {
        collisionEnabled = true;
    }
    
    public final boolean
    isCollidable()
    {
        return collisionEnabled;
    }
    
    protected abstract void
    onCollisionEvent(
            CollisionEvent ce);
    
    protected abstract void
    onCollisionEnter(
            CollisionEvent ce);
    
    protected abstract void
    onCollisionExit(
            CollisionEvent ce);
}
