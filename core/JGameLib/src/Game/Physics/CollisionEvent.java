/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

import java.awt.geom.Area;

/**
 *
 * @author daniel
 */
public class CollisionEvent
{
    public String groupName;
    public Collidable other;
    public Area area;
    
    public CollisionEvent(
            String collisionGroupName,
            Collidable other,
            Area collisionArea)
    {
        groupName = collisionGroupName;
        this.other = other;
        area = collisionArea;
    }
}
