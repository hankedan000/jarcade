/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

/**
 *
 * @author daniel
 */
public interface Simulatable
{
    /**
     * Description: Method called when the object should simulate it's motion
     * 
     * @param dtMillis elapsed time in milliseconds since last call
     */
    public void
    sim(
            double dtMillis);
}
