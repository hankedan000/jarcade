/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

/**
 *
 * @author daniel
 */
public class CollisionEngine implements OnSimListener {
    private final HashMap<String, CollisionGroup> groups;
    Stack<Collidable> markedForRemoval;

    public CollisionEngine() {
        groups = new HashMap<>();
        markedForRemoval = new Stack<>();
    }

    public CollisionGroup getCollisionGroup(String name) {
        if (groups.containsKey(name)) {
            return groups.get(name);
        } else {
            CollisionGroup newGroup = new CollisionGroup(name);
            groups.put(name, newGroup);
            return newGroup;
        }
    }

    public Set<Entry<String, CollisionGroup>> getCollisionGroups() {
        return groups.entrySet();
    }
    
    public void removeCollidable(Collidable c) {
        markedForRemoval.push(c);
    }

    @Override
    public void onSimBegin() {
        // do nothing
    }

    @Override
    public void onSimComplete() {
        for (CollisionGroup group : groups.values()) {
            group.checkAndNotify();
        }
        
        garbageCollectCollidables();
    }
    
    private void garbageCollectCollidables() {
        while (!markedForRemoval.empty()) {
            Collidable c = markedForRemoval.pop();
            for (Entry<String,CollisionGroup> e : groups.entrySet()) {
                e.getValue().removeCollidable(c);
            }
        }
    }

}
