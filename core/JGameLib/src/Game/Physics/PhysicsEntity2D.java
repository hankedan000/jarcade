/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

import Game.Primitives.Vector2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

/**
 *
 * @author daniel
 */
public abstract class PhysicsEntity2D extends Collidable
                                      implements Simulatable,
                                                 OnSimListener
{
    boolean isStatic = false;
    
    // Position vectory of the entity
    protected Vector2D s_ = new Vector2D();
    
    // Velocity vectory of the entity
    protected Vector2D v_ = new Vector2D();
    
    // Acceleration vectory of the entity
    protected Vector2D a_ = new Vector2D();
    
    public final void
    setStatic(boolean isStatic)
    {
        this.isStatic = isStatic;
    }
    
    public final boolean
    isStatic()
    {
        return isStatic;
    }
    
    public final void
    setPosition(
            Vector2D s)
    {
        s_ = s;
    }
    
    public final void
    setVelocity(
            Vector2D v)
    {
        v_ = v;
    }
    
    public final void
    setAcceleration(
            Vector2D a)
    {
        a_ = a;
    }
    
    public final Vector2D
    getPosition()
    {
        return s_;
    }
    
    public final Vector2D
    getVelocity()
    {
        return v_;
    }
    
    public final Vector2D
    getAcceleration()
    {
        return a_;
    }
    
    @Override
    public void
    sim(
            double dtMillis)
    {
        if(!isStatic)
        {
            v_ = v_.add(a_.scale(dtMillis/1000.0));
            s_ = s_.add(v_.scale(dtMillis/1000.0));
        }
    }

    @Override
    protected void
    onCollisionEvent(
            CollisionEvent ce)
    {
        // do nothing
    }

    @Override
    protected void
    onCollisionExit(
            CollisionEvent ce)
    {
        // do nothing
    }

    @Override
    protected void
    onCollisionEnter(
            CollisionEvent ce)
    {
        // do nothing
    }

    @Override
    public Area getGlobalBounds()
    {
        Area local = getLocalBounds();
        
        AffineTransform at = new AffineTransform();
        at.translate(s_.dX, s_.dY);
        local.transform(at);
        
        return local;
    }
    
    protected abstract Area
    getLocalBounds();

    @Override
    public void onSimBegin() {
        /**
         * This method is called right before the PhysicsEngine calls sim().
         * This base class doesn't have anything important to perform here, but
         * if the subclass wishes, it can override this implementation to have
         * the callback.
         */
    }

    @Override
    public void onSimComplete() {
        /**
         * This method is called right after the PhysicsEngine calls sim().
         * This base class doesn't have anything important to perform here, but
         * if the subclass wishes, it can override this implementation to have
         * the callback.
         */
    }
}
