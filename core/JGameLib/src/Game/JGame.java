/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import Game.Data.GameData;
import Game.Data.GameDataService;
import Game.Data.GameInfo;
import Game.Draw.Drawable;
import Game.Draw.GamePanel;
import Game.GameStateListener.State;
import Game.Physics.CollisionEngine;
import Game.Physics.OnSimListener;
import Game.Physics.PhysicsEngine;
import Game.Primitives.GameObject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import Game.Draw.RendererInterface;
import Game.Draw.ResizeEvent;
import Game.Services.GameServices;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public abstract class JGame implements RendererInterface,
                                       SpawnerInterface,
                                       OnSimListener,
                                       KeyListener,
                                       GameObject.GameObjectLifeListener
{
    private static final int    FPS = 50;
    public  static final Random rand = new Random(System.nanoTime());
    
    // Used to make sure that only 1 thread can modify the GameObject containers
    private final Semaphore               objectLock = new Semaphore(1,true);
    private final CollisionEngine         collisionEngine;
    private final PhysicsEngine           physicsEngine;
    private final List<Drawable>          objectsToRender;
    private final List<GameStateListener> stateListeners;
    private final List<KeyListener>       keyListeners;
    private final List<GamePanel>         gamePanels;
    
    private boolean paused;

    public JGame()
    {
        collisionEngine = new CollisionEngine();
        physicsEngine = new PhysicsEngine(FPS);
        objectsToRender = new ArrayList<>();
        stateListeners = new ArrayList<>();
        keyListeners = new ArrayList<>();
        gamePanels = new ArrayList<>();
        
        paused = false;
    }
    
    public void
    boot(
            GameServices gs)
    {
        // Increment the JGame's play counter
        GameDataService gds = gs.getGameDataService();
        GameData gd = gds.load(getGameInfo());
        gd.incrementPlayCount();
        if (!gds.save(gd))
        {
            System.err.println("Failed to save GameData!");
        }
        
        stateListeners.add(physicsEngine);
        physicsEngine.registerOnSimListener(collisionEngine);
        
        // We do the repainting on the onSimComplete();
        physicsEngine.registerOnSimListener(this);
        
        this.notifyStateListeners(State.eBOOT);
    }
    
    public void
    shutdown()
    {
        paused = true;
        this.notifyStateListeners(State.eSHUTDOWN);
    }
    
    public RendererInterface
    getRenderer()
    {
        return this;
    }
    
    protected final void
    addGamePanel(
            GamePanel panel)
    {
        gamePanels.add(panel);
    }
    
    @Override
    public final void
    addObject(
            GameObject object)
    {
        object.setGameObjectLifeListener(this);
        addDrawable(object);
        physicsEngine.addEntity(object);
        addGameStateListener(object);
    }

    @Override
    public void 
    removeObject(
            GameObject object)
    {
        removeDrawable(object);
        physicsEngine.removeEntity(object);
        collisionEngine.removeCollidable(object);
        removeGameStateListener(object);
    }
    
    @Override
    public final void
    addObjects(
            Collection<GameObject> objects)
    {
        this.addDrawables((List)objects);
        physicsEngine.addEntities((List)objects);
        for (GameObject go : objects) {
            addGameStateListener(go);
        }
    }
    
    @Override
    public final void
    addDrawable(
            Drawable drawable)
    {
        objectsToRender.add(drawable);
    }
    
    public void addGameStateListener(GameStateListener gsl) {
        stateListeners.add(gsl);
    }
    
    public void removeGameStateListener(GameStateListener gsl) {
        try {
            objectLock.acquire();
            stateListeners.remove(gsl);
            objectLock.release();
        } catch(InterruptedException ex) {
            Logger.getLogger(JGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public final void
    removeDrawable(
            Drawable drawable)
    {
        try {
            objectLock.acquire();
            objectsToRender.remove(drawable);
            objectLock.release();
        } catch (InterruptedException ex) {
            Logger.getLogger(JGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public final void
    addDrawables(
            Collection<Drawable> drawables)
    {
        objectsToRender.addAll(drawables);
    }
    
    @Override
    public int
    getFramesPerSecond()
    {
        return FPS;
    }
    
    public CollisionEngine
    getCollisionEngine()
    {
        return collisionEngine;
    }
    
    public final PhysicsEngine
    getPhysicsEngine()
    {
        return physicsEngine;
    }

    public void
    pause()
    {
        paused = true;
        this.notifyStateListeners(State.ePAUSE);
    }

    public void
    resume()
    {
        paused = false;
        this.notifyStateListeners(State.eRESUME);
    }

    @Override
    public List<Drawable>
    getDrawables()
    {
        return new ArrayList<>(objectsToRender);
    }

    public boolean
    isPaused()
    {
        return paused;
    }

    @Override
    public void
    onSimBegin()
    {
        // don't care
    }

    @Override
    public void
    onSimComplete()
    {
        /**
         * Note:
         * The Game should always be notified of completion LAST! This is
         * because the the physics engine put it's registered listeners in a
         * stack. Since the Game is always the first to register, it will
         * always be notified last. Make sense?!? This is nice for things that
         * might want to update their position in the onSimComplete() before
         * the repaint occurs.
         */
        for(GamePanel panel : gamePanels)
        {
            panel.repaint();
        }
    }

    @Override
    public void
    notifyResize(
            ResizeEvent re)
    {
        try {
            objectLock.acquire();
            for(Drawable d : objectsToRender)
            {
                d.onResize(re);
            }
            objectLock.release();
        } catch (InterruptedException ex) {
            Logger.getLogger(JGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public abstract GameInfo
    getGameInfo();
    
    public List<GamePanel>
    getGamePanels()
    {   
        return gamePanels;
    }

    public final List<KeyListener>
    getKeyListeners()
    {
        return new ArrayList<>(keyListeners);
    }
    
    public final void
    addKeyListener(
            KeyListener keyListener)
    {
        keyListeners.add(keyListener);
    }
    
    @Override
    public final void
    keyTyped(
            KeyEvent ke)
    {
        for (KeyListener l : keyListeners)
        {
            l.keyTyped(ke);
        }
    }

    @Override
    public final void
    keyPressed(
            KeyEvent ke)
    {
        for (KeyListener l : keyListeners)
        {
            l.keyPressed(ke);
        }
    }

    @Override
    public final void
    keyReleased(
            KeyEvent ke)
    {
        for (KeyListener l : keyListeners)
        {
            l.keyReleased(ke);
        }
    }
    
    private void
    notifyStateListeners(
            GameStateListener.State state)
    {
        try {
            objectLock.acquire();
            for(GameStateListener listener : stateListeners)
            {
                switch(state)
                {
                    case eBOOT:
                        listener.onBoot();
                        break;
                    case eSHUTDOWN:
                        listener.onShutdown();
                        break;
                    case ePAUSE:
                        listener.onPause();
                        break;
                    case eRESUME:
                        listener.onResume();
                        break;
                }
            }
            objectLock.release();
        } catch (InterruptedException ex) {
            Logger.getLogger(JGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onObjectKilled(GameObject object) {
        removeObject(object);
    }
}
