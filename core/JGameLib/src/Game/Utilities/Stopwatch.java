/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class Stopwatch {
    class Record {
        long min;
        long max;
        long elapsed;
        long count;
        private long prevStart_ns_;
        private boolean isStarted_;

        public Record() {
            reset();
        }
        
        public void reset() {
            min = Long.MAX_VALUE;
            max = Long.MIN_VALUE;
            elapsed = 0;
            count = 0;
            prevStart_ns_ = 0;
            isStarted_ = false;
        }
        
        public void start() {
            prevStart_ns_ = System.nanoTime();
            isStarted_ = true;
        }
        
        public void stop() {
            if(isStarted_) {
                add(System.nanoTime() - prevStart_ns_);
            }
            isStarted_ = false;
        }
        
        private void add(long dur) {
            min = Math.min(min, dur);
            max = Math.max(max, dur);
            elapsed += dur;
            count++;
        }
        
        public String getSummaryString() {
            long mean = 0;
            if (count > 0) {
                mean = elapsed / count;
            }
            return String.format(
                    "mean:    %.3fus\n"
                  + "min:     %.3fus\n"
                  + "max:     %.3fus\n"
                  + "elapsed: %.3fus\n"
                  + "count:   %d\n",
                    mean / 1000.0,
                    min / 1000.0,
                    max / 1000.0,
                    elapsed / 1000.0,
                    count);
        }
    }
    
    private Map<String, Record> records_;
    private boolean enabled_;
    
    public Stopwatch() {
        records_ = new HashMap<>();
        enabled_ = true;
        reset();
    }
    
    public void disable() {
        enabled_ = false;
    }
    
    public void createRecord(String recordName) {
        records_.put(recordName, new Record());
    }
    
    public void reset() {
        if (!enabled_) {
            return;
        }
        
        for (Record r : records_.values()) {
            r.reset();
        }
    }
    
    public void start(String recordName) {
        if (!enabled_) {
            return;
        }
        
        Record r = records_.get(recordName);
        if (r != null) {
            r.start();;
        }
    }
    
    public void stop(String recordName) {
        if (!enabled_) {
            return;
        }
        
        Record r = records_.get(recordName);
        if (r != null) {
            r.stop();;
        }
    }
    
    public void logSummary() {
        records_.forEach(new BiConsumer<String, Record>() {
            @Override
            public void accept(String name, Record record) {
                System.out.printf("*** %s ***\n%s",
                                  name,
                                  record.getSummaryString());
            }
        });
    }
}
