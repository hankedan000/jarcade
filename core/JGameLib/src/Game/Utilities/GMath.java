/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Utilities;

/**
 *
 * @author daniel
 */
public class GMath
{
    public static double
    map(double val,
        double fromMin, double fromMax,
        double toMin,   double toMax)
    {
        return toMin + (toMax - toMin) * ((val - fromMin) / (fromMax - fromMin));
    }
}
