/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Utilities;

import Game.Draw.ViewPort.OriginPosition;
import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author daniel
 */
public class ShapeUtils
{
    // hide constructor
    private ShapeUtils()
    {}
    
    public static Polygon
    getPolyRectangle(
            int width,
            int height,
            OriginPosition originPosition)
    {
        Polygon rect = new Polygon();
        rect.addPoint(0,     0);
        rect.addPoint(width, 0);
        rect.addPoint(width, height);
        rect.addPoint(0,     height);
        
        if(originPosition == OriginPosition.eCentered)
        {
            rect.translate(-width / 2, -height / 2);
        }
        
        return rect;
    }
    
    public static Rectangle2D
    getRectangle(
            int width,
            int height,
            OriginPosition originPosition)
    {
        Rectangle2D rect;
        
        if(originPosition == OriginPosition.eCentered)
        {
            rect = new Rectangle2D.Double(
                    width  / -2,// x
                    height / -2,// y
                    width,      // w
                    height);    // h
        }
        else
        {
            rect = new Rectangle2D.Double(
                    0,      // x
                    0,      // y
                    width,  // w
                    height);// h
        }
        
        return rect;
    }
    
    public static Ellipse2D
    getEllipse(
            int width,
            int height,
            OriginPosition originPosition)
    {
        int x = 0,y = 0;
        if(originPosition == OriginPosition.eCentered)
        {
            x = width  / -2;
            y = height / -2;
        }
        
        return new Ellipse2D.Double(x, y, width, height);
    }
}
