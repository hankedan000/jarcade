/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Services;

import Game.Data.GameDataService;

/**
 *
 * @author daniel
 */
public interface GameServices
{

    /**
     * Provides access to a GameDataService, allowing games to save and load
     * GameData.
     * 
     * @return
     * Returns a provider of the GameDataService interface
     */
    public GameDataService
    getGameDataService();
}
