/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game;

import Game.Primitives.GameObject;
import java.util.Collection;

/**
 *
 * @author daniel
 */
public interface SpawnerInterface
{
    public void
    addObject(
            GameObject object);
    
    public void
    removeObject(
            GameObject object);
    
    public void
    addObjects(
            Collection<GameObject> objects);
}
