/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Primitives;

import Game.Draw.Drawable;
import Game.GameStateListener;
import Game.Physics.PhysicsEntity2D;

/**
 *
 * @author daniel
 */
public abstract class GameObject extends PhysicsEntity2D
                                     implements GameStateListener,
                                                Drawable
{
    GameObjectLifeListener gameObjectLifeListener;
    
    public void setGameObjectLifeListener(GameObjectLifeListener l) {
        gameObjectLifeListener = l;
    }
    
    /**
     * This method is used to destroy a game object. It will trigger a series
     * of events that will purge the object from the JGame.
     * 
     * Note: this method can NOT be called inside of GameObject::sim(), or else
     * it will cause the game thread to halt due to a Semaphore.acquire() block
     */
    public void kill() {
        if (gameObjectLifeListener != null) {
            gameObjectLifeListener.onObjectKilled(this);
        }
    }
    
    @Override
    public void
    onPause()
    {
        // do nothing
    }

    @Override
    public void
    onResume()
    {
        // do nothing
    }

    @Override
    public void
    onBoot()
    {
        // do nothing
    }

    @Override
    public void
    onShutdown()
    {
        // do nothing
    }
    
    public interface GameObjectLifeListener {
        void onObjectKilled(GameObject object);
    }
}
