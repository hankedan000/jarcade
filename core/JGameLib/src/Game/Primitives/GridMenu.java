/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Primitives;

import Game.Data.PlayerControls;
import Game.Data.GlobalConfig;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author daniel
 */
public class GridMenu extends JFrame
                      implements KeyListener
{
    private final GridBagLayout layout;
    private final PlayerControls controls;
    private final int rows;
    private final int cols;

    public
    GridMenu(
            PlayerControls playersControls,
            int gridRows,
            int gridCols)
    {
        layout = new GridBagLayout();
        controls = playersControls;
        setLayout(layout);
        rows = gridRows;
        cols = gridCols;

        // Remove menu buttons
        setUndecorated(true);
        // Center in display
        setLocationRelativeTo(null);
    }

    public void
    addComponent(
            int gridX,
            int gridY,
            Component c)
    {
        addComponent(gridX, gridY, 1, 1, c);
    }

    public void
    addComponent(
            int gridX,
            int gridY,
            int gridWidth,
            int gridHeight,
            Component c)
    {
        if (gridX < cols && gridY < rows)
        {
            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx      = gridX;
            gbc.gridy      = gridY;
            gbc.gridwidth  = gridWidth;
            gbc.gridheight = gridHeight;
            gbc.fill       = GridBagConstraints.BOTH;
            c.addKeyListener(this);
            add(c, gbc);
        }
        else
        {
            Logger.getLogger(GridMenu.class.getName()).log(Level.SEVERE,
                    "gridX or gridY was out of bound");
        }
    }

    public static void main(String[] args)
    {
//        GridMenu menu = new GridMenu(GlobalConfig.player1Controls, 4, 4);
//        menu.setSize(200, 400);
//        menu.addComponent(0, 0, 2, 1, new JButton("0"));
//        menu.addComponent(2, 0, 2, 1, new JButton("1"));
//        menu.addComponent(0, 1, new JButton("2"));
//        menu.addComponent(1, 1, new JButton("3"));
//        menu.addComponent(2, 1, new JButton("4"));
//        menu.addComponent(3, 1, new JButton("5"));
//        menu.addComponent(0, 2, 2, 2, new JButton("6"));
//        menu.addComponent(2, 2, new JButton("7"));
//        menu.addComponent(3, 3, new JButton("8"));
//        menu.setVisible(true);
    }

    @Override
    public void
    keyTyped(
            KeyEvent ke)
    {
    }

    @Override
    public void
    keyPressed(
            KeyEvent ke)
    {
        if (ke.getKeyCode() == controls.UP)
        {
//            System.out.println("UP");
            menuUp();
        }
        else if (ke.getKeyCode() == controls.DOWN)
        {
//            System.out.println("DOWN");
            menuDown();
        }
        else if (ke.getKeyCode() == controls.LEFT)
        {
//            System.out.println("LEFT");
            menuLeft();
        }
        else if (ke.getKeyCode() == controls.RIGHT)
        {
//            System.out.println("RIGHT");
            menuRight();
        }
//        else if (ke.getKeyCode() == GlobalConfig.EXIT)
//        {
//            this.dispose();
//        }
    }

    @Override
    public void
    keyReleased(
            KeyEvent ke)
    {
    }
    
    private Component
    getComponent(
            int x,
            int y)
    {
        for (Component c : getContentPane().getComponents())
        {
            GridBagConstraints gbc = layout.getConstraints(c);
            if (gbc.gridx == x && gbc.gridy == y)
            {
                return c;
            }
        }
        
        return null;
    }
    
    private void
    menuUp()
    {
        Component curComp = getFocusOwner();
        GridBagConstraints gbc = layout.getConstraints(curComp);
        
        Component newComp = getComponent(gbc.gridx, gbc.gridy - 1);
        if (newComp != null)
        {
            newComp.requestFocus();
        }
    }
    
    private void
    menuDown()
    {
        Component curComp = getFocusOwner();
        GridBagConstraints gbc = layout.getConstraints(curComp);
        
        Component newComp = getComponent(gbc.gridx, gbc.gridy + 1);
        if (newComp != null)
        {
            newComp.requestFocus();
        }
    }
    
    private void
    menuLeft()
    {
        Component curComp = getFocusOwner();
        GridBagConstraints gbc = layout.getConstraints(curComp);
        
        Component newComp = getComponent(gbc.gridx - 1, gbc.gridy);
        if (newComp != null)
        {
            newComp.requestFocus();
        }
    }
    
    private void
    menuRight()
    {
        Component curComp = getFocusOwner();
        GridBagConstraints gbc = layout.getConstraints(curComp);
        
        Component newComp = getComponent(gbc.gridx + 1, gbc.gridy);
        if (newComp != null)
        {
            newComp.requestFocus();
        }
    }
}
