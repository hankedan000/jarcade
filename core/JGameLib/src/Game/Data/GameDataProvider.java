/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author daniel
 */
public class GameDataProvider implements GameDataService
{
    private static GameDataProvider instance = null;
    private static GlobalConfig globalConfig = new GlobalConfig();
    private static final HashMap<String,GameData> gameDatas = new HashMap<>();
    
    protected GameDataProvider()
    {
        restoreFromFileSystem();
    }
    
    public static GameDataProvider
    getInstance()
    {
        if (instance == null)
        {
            instance = new GameDataProvider();
        }
        return instance;
    }
    
    @Override
    public boolean
    save(
            GameData gd)
    {
        return save((Savable)gd);
    }
    
    public boolean
    save(
            GlobalConfig gc)
    {
        globalConfig = gc;
        return save((Savable)gc);
    }
    
    @Override
    public GameData
    load(
            GameInfo gi)
    {
        // Does the gamedata already exists in the filesystem
        if (gameDatas.containsKey(gi.getName()))
        {// data exists, so return the data
            return gameDatas.get(gi.getName());
        }
        else
        {// game is new, so create one
            GameData gd = new GameData(gi);
            gd.setPlayer1Controls(globalConfig.player1Controls);
            gd.setPlayer2Controls(globalConfig.player2Controls);
            gameDatas.put(gi.getName(), gd);
            return gd;
        }
    }

    public List<GameData>
    getGameDatas()
    {
        return (List)gameDatas.values();
    }
    
    private static File
    getJArcadeRoot()
    {
        File root = null;
        
        if (isLinux())
        {
            root = new File(System.getProperty("user.home") + "/.jarcade");
        }
        else if (isWindows())
        {
            root = new File("C:\\Program Files\\JArcade");
        }
        else
        {
            System.err.println("This OS isn't supported by JArcade yet!");
        }
        
        if (root != null && !root.exists())
        {
            root.mkdir();
        }
        
        return root;
    }
    
    private static File
    getGameDataRoot()
    {
        File gdRoot = new File(
                getJArcadeRoot() + File.separator + GameData.SUBDIR);
        if (!gdRoot.exists())
        {
            gdRoot.mkdir();
        }
        
        return gdRoot;
    }
    
    private static boolean
    isLinux()
    {
        String osName = System.getProperty("os.name");
        return osName.compareTo("Linux") == 0;
    }
    
    private static boolean
    isWindows()
    {
        String osName = System.getProperty("os.name");
        return osName.contains("Windows");
    }
    
    private File
    getFullPath(
            Savable s)
    {   
        String fullpath = "";
        fullpath += getJArcadeRoot() + File.separator;
        if (!s.getSubDirectory().isEmpty())
        {
            fullpath += s.getSubDirectory() + File.separator;
        }
        fullpath += s.getFullFilename();
        
        return new File(fullpath);
    }
    
    private boolean
    save(
            Savable saveable)
    {
        boolean saved = true;
        File file = getFullPath(saveable);
        
        try
        {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            // Serialize config to file
            oos.writeObject(saveable);
            
            oos.close();
            fos.close();
        }
        catch (FileNotFoundException ex)
        {
            Logger.getLogger(GameDataProvider.class.getName()).log(Level.SEVERE, null, ex);
            saved = false;
        }
        catch (IOException ex)
        {
            Logger.getLogger(GameDataProvider.class.getName()).log(Level.SEVERE, null, ex);
            saved = false;
        }
        
        return saved;
    }
    
    private void
    restoreFromFileSystem()
    {
        // Restore GlobalConfig
        File gcFile = new File(getJArcadeRoot() +
                               File.separator +
                               globalConfig.getFullFilename());
        globalConfig = restoreGlobalConfig(gcFile);
        
        // Restore individual GameDatas
        File gdDir = getGameDataRoot();
        
        File[] dats = gdDir.listFiles();
        for(File datFile : dats)
        {
            GameData gd = restoreGameData(datFile);
            gameDatas.put(gd.getGameName(), gd);
        }
    }
    
    private GlobalConfig
    restoreGlobalConfig(
            File gcFile)
    {
        GlobalConfig gc = null;
        
        try
        {
            FileInputStream fis = new FileInputStream(gcFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            // Deserialize GlobalConfig
            gc = (GlobalConfig)ois.readObject();
            
            ois.close();
            fis.close();
        }
        catch (FileNotFoundException ex)
        {
            gc = new GlobalConfig();
            gc.setDefaults();
            save(gc);
        } catch (IOException ex)
        {
            Logger.getLogger(GameDataProvider.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex)
        {
            Logger.getLogger(GameDataProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return gc;
    }
    
    private GameData
    restoreGameData(
            File gdFile)
    {
        GameData gd = null;
        
        try
        {
            FileInputStream fis = new FileInputStream(gdFile);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            // Deserialize GameData
            gd = (GameData)ois.readObject();
            
            ois.close();
            fis.close();
        }
        catch (FileNotFoundException ex)
        {
            Logger.getLogger(GameDataProvider.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(GameDataProvider.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex)
        {
            Logger.getLogger(GameDataProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return gd;
    }
}
