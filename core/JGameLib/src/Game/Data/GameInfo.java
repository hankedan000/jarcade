/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import Game.Draw.PanelPlacement;
import java.awt.Color;

/**
 *
 * @author daniel
 */
public interface GameInfo
{
    public String getName();
    
    public PanelPlacement getPanelPlacement();
    
    /**
     * Getter for the game's required screen aspect ratio
     * 
     * @return
     * The ratio of screen width to screen height (width/height)
     */
    public double getAspectRatio();
    
    /**
     * Getter for the games desired color for the bezel
     * 
     * The bezel is the portion of the screen around the GamePanel when the
     * GamePanel doesn't completely fill in the entire screen.
     * 
     * @return
     * the desired bezel color
     */
    public Color getBezelColor();
    
    public int minPlayers();
    
    public int maxPlayers();
}
