/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

/**
 *
 * @author daniel
 */
public interface GameDataService
{
    public boolean
    save(
            GameData gd);
    
    public GameData
    load(
            GameInfo gi);
}
