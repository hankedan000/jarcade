/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author daniel
 */
public class Scoreboard implements Serializable
{
    private static final long serialVersionUID = 3000L;
    public static final int MAX_HIGHSCORES = 10;
    private List<Highscore> highscores;
    
    public Scoreboard()
    {
        highscores = new ArrayList<>();
    }
    
    public void
    addHighscore(
            long score,
            String name)
    {
        if (isHighscore(score))
        {
            highscores.add(new Highscore(score, name));
            Collections.sort(highscores, Collections.reverseOrder());
            highscores = highscores.subList(
                    0,
                    Math.min(MAX_HIGHSCORES, highscores.size()));
        }
    }
    
    public boolean
    isHighscore(
            long score)
    {
        if(highscores.size() > 0)
        {
            return score > highscores.get(highscores.size() - 1).getScore();
        }
        else
        {
            return true;
        }
    }
    
    public List<Highscore>
    getHighscores()
    {
        return new ArrayList<Highscore>(highscores);
    }
    
    public String
    toString()
    {
        String str = "";
        
        for (int hh=0; hh<highscores.size(); hh++)
        {
            str += String.format(
                    "%d. %s\n",
                    hh + 1,
                    highscores.get(hh).toString());
        }
        
        return str;
    }
}
