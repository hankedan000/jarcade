/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import java.io.Serializable;

/**
 *
 * @author daniel
 */
abstract class Savable implements Serializable
{
    abstract String
    getSubDirectory();
    
    abstract String
    getFileExtension();
    
    abstract String
    getFilename();
    
    String
    getFullFilename()
    {
        return String.format("%s.%s", getFilename(), getFileExtension());
    }
}
