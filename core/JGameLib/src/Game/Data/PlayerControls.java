/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import java.io.Serializable;

/**
 *
 * @author daniel
 */
public class PlayerControls implements Serializable
{
    private static final long serialVersionUID = 1000L;
    public int UP;
    public int DOWN;
    public int LEFT;
    public int RIGHT;
    public int BTN1;
    
    public enum ArcadeButton {
        eUP,eDOWN,eLEFT,eRIGHT,eBTN1
    }
}
