/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import java.awt.event.KeyEvent;

/**
 *
 * @author daniel
 */
public class GlobalConfig extends Savable
{
    private static final long serialVersionUID = 2000L;
    protected static String FILE_NAME = "jarcade";
    protected static String FILE_EXTENSION = "cfg";
    public int EXIT;
    public int P1_START;
    public int P2_START;
    
    public PlayerControls player1Controls;
    public PlayerControls player2Controls;
    
    public GlobalConfig()
    {
    }
    
    public void
    setDefaults()
    {
        EXIT = KeyEvent.VK_ESCAPE;
        P1_START = KeyEvent.VK_1;
        P2_START = KeyEvent.VK_2;
        
        player1Controls = getDefaultPlayer1();
        player2Controls = getDefaultPlayer2();
    }

    public static PlayerControls
    getDefaultPlayer1()
    {
        PlayerControls p1 = new PlayerControls();
        
        p1.BTN1  = KeyEvent.VK_SPACE;
        p1.UP    = KeyEvent.VK_UP;
        p1.DOWN  = KeyEvent.VK_DOWN;
        p1.LEFT  = KeyEvent.VK_LEFT;
        p1.RIGHT = KeyEvent.VK_RIGHT;
        
        return p1;
    }

    public static PlayerControls
    getDefaultPlayer2()
    {
        PlayerControls p2 = new PlayerControls();
        
        p2.BTN1  = KeyEvent.VK_CONTROL;
        p2.UP    = KeyEvent.VK_W;
        p2.DOWN  = KeyEvent.VK_A;
        p2.LEFT  = KeyEvent.VK_S;
        p2.RIGHT = KeyEvent.VK_D;
        
        return p2;
    }
    
    @Override
    String getSubDirectory()
    {
        return "";
    }

    @Override
    public String getFileExtension()
    {
        return FILE_EXTENSION;
    }

    @Override
    public String getFilename()
    {
        return FILE_NAME;
    }
}
