/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author daniel
 */
public class Highscore implements Comparable<Highscore>
{
    private final long   score;
    private final String name;
    private final long   unixDate;
    
    public Highscore(
            long score,
            String name)
    {
        this.score = score;
        this.name = name;
        unixDate = System.currentTimeMillis() / 1000L;
    }
    
    public long
    getScore()
    {
        return score;
    }
    
    public String
    getName()
    {
        return name;
    }
    
    public static void main(String[] args) {
        Highscore hi = new Highscore(1100, "DJH");
        System.out.println(hi.toString(true));
    }

    @Override
    public String
    toString()
    {
        return toString(false);
    }
    
    public String
    toString(
            boolean showDate)
    {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date(unixDate * 1000L);
        String str = String.format(
                "%d %s %s",
                score,
                name,
                (showDate ? df.format(date) : ""));
        
        return str;
    }

    @Override
    public int
    compareTo(
            Highscore other)
    {
        return Long.compare(this.score, other.score);
    }
}
