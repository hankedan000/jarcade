/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

/**
 *
 * @author daniel
 */
public class GameData extends Savable
{
    private static final long serialVersionUID = 4000L;
    protected static String FILE_EXTENSION = "gdat";
    protected static String SUBDIR         = "gamedata";
    private final String     gameName;
    private final Scoreboard scoreboard;
    private PlayerControls   p1Controls;
    private PlayerControls   p2Controls;
    private long             playCounter;
    
    public GameData(
            GameInfo gi)
    {
        gameName = gi.getName();
        if (gameName == null || gameName.isEmpty())
        {
            throw new IllegalArgumentException(
                    "gameName can not be null or empty!");
        }
        
        scoreboard  = new Scoreboard();
        playCounter = 0;
        p1Controls  = GlobalConfig.getDefaultPlayer1();
        p2Controls  = GlobalConfig.getDefaultPlayer2();
    }
    
    public void
    setPlayer1Controls(
            PlayerControls pc)
    {
        p1Controls = pc;
    }
    
    public void
    setPlayer2Controls(
            PlayerControls pc)
    {
        p2Controls = pc;
    }
    
    public String
    getGameName()
    {
        return gameName;
    }
    
    public Scoreboard
    getScoreboard()
    {
        return scoreboard;
    }
    
    public PlayerControls
    getPlayer1Controls()
    {
        return p1Controls;
    }
    
    public PlayerControls
    getPlayer2Controls()
    {
        return p2Controls;
    }
    
    public long
    getPlayCount()
    {
        return playCounter;
    }
    
    public void
    incrementPlayCount()
    {
        playCounter++;
    }

    @Override
    String getSubDirectory()
    {
        return SUBDIR;
    }

    @Override
    public String
    getFileExtension()
    {
        return FILE_EXTENSION;
    }

    @Override
    public String
    getFilename()
    {
        return gameName;
    }
}
