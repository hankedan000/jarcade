/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Data;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daniel
 */
public class ScoreboardTest {
    
    public ScoreboardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of addHighscore method, of class Scoreboard.
     */
    @Test
    public void testAddHighscore() {
        System.out.println("addHighscore");
        long score = 10L;
        String name = "Test Add";
        Scoreboard sb = new Scoreboard();
        sb.addHighscore(score, name);
        assertEquals(sb.getHighscores().size(), 1);
        assertEquals(sb.getHighscores().get(0).getScore(), 10L);
        assertEquals(sb.getHighscores().get(0).getName(), "Test Add");
    }

    /**
     * Test of isHighscore method, of class Scoreboard.
     */
    @Test
    public void testIsHighscore() {
        System.out.println("isHighscore");
        Scoreboard sb = new Scoreboard();
        for(int i=0; i<Scoreboard.MAX_HIGHSCORES; i++)
        {
            long score = i * 1000;
            assertTrue(sb.isHighscore(score));
            sb.addHighscore(score, "High" + i);
        }
        assertFalse(sb.isHighscore(0L));
    }

    /**
     * Test of getHighscores method, of class Scoreboard.
     */
    @Test
    public void testGetHighscores() {
        System.out.println("getHighscores");
        Scoreboard sb = new Scoreboard();
        for(int i=0; i<Scoreboard.MAX_HIGHSCORES; i++)
        {
            long score = i * 1000;
            sb.addHighscore(score, "High" + i);
            assertEquals(sb.getHighscores().size(), i + 1);
            assertEquals(sb.getHighscores().get(0).getScore(), score);
        }
        sb.addHighscore(Scoreboard.MAX_HIGHSCORES * 5000, "TopDog");
        assertEquals(sb.getHighscores().size(), Scoreboard.MAX_HIGHSCORES);
    }
}
