/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Physics;

import Game.JGame;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daniel
 */
public class PhysicsEngineTest
{
    private static final int FPS = 50;
    public PhysicsEngineTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of registerOnSimListener method, of class PhysicsEngine.
     */
    @Test
    public void testRegisterOnSimListener() {

        System.out.println("registerOnSimListener");

        OnSimListenerImpl listener = new OnSimListenerImpl();
        PhysicsEngine instance = new PhysicsEngine(FPS);
        instance.onBoot();
        instance.registerOnSimListener(listener);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            fail("Interrupted before test finished!");
        }
        
        assertEquals(true, listener.onSimBeginCalled);
        assertEquals(true, listener.onSimCompleteCalled);
    }
    
    private class SimulatableImpl implements Simulatable
    {
        protected boolean simulated = false;
        
        @Override
        public void sim(double dtMillis) {
            simulated = true;
        }
    }
    
    private class OnSimListenerImpl implements OnSimListener
    {
        protected boolean onSimBeginCalled = false;
        protected boolean onSimCompleteCalled = false;
        
        @Override
        public void onSimBegin() {
            onSimBeginCalled = true;
        }

        @Override
        public void onSimComplete() {
            onSimCompleteCalled = true;
        }
    }
}
