/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JArcade;

import Game.Data.PlayerControls;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author daniel
 */
public class PlayerControlsConfigTool extends JDialog
                                      implements KeyListener
{
    private final JLabel msg;
    private int playerBeingConfigured;
    private ConfigState currentState;
    private ConfigState nextState;
    private List<PlayerControls> controls;
    
    private enum ConfigState
    {
        eUP,eDOWN,eLEFT,eRIGHT,eBTN1;
    }
    
    public PlayerControlsConfigTool(
            JFrame parent,
            int numPlayers)
    {
        super(parent,true);
        setTitle("Player Controls Configuration Tool");
        setPreferredSize(new Dimension(300,100));
        msg = new JLabel();
        getContentPane().add(msg);
        addKeyListener(this);
        pack();
        
        playerBeingConfigured = 0;
        currentState = ConfigState.eUP;
        controls = new ArrayList<>();
        for(int i=0; i<numPlayers; i++)
        {
            controls.add(new PlayerControls());
        }
    }
    
//    public static void main(String[] args) {
//        PlayerControlsConfigTool tool = new PlayerControlsConfigTool(2);
//        tool.showDialog();
//    }
    
    public void
    showDialog()
    {
        msg.setText(getMsg());
        setVisible(true);
    }
    
    public List<PlayerControls>
    getControls()
    {
        return controls;
    }
    
    private void
    configure(
            int keyCode)
    {
        switch(currentState)
        {
            case eUP:
                nextState = ConfigState.eDOWN;
                controls.get(playerBeingConfigured).UP = keyCode;
                break;
            case eDOWN:
                nextState = ConfigState.eLEFT;
                controls.get(playerBeingConfigured).DOWN = keyCode;
                break;
            case eLEFT:
                nextState = ConfigState.eRIGHT;
                controls.get(playerBeingConfigured).LEFT = keyCode;
                break;
            case eRIGHT:
                nextState = ConfigState.eBTN1;
                controls.get(playerBeingConfigured).RIGHT = keyCode;
                break;
            case eBTN1:
                nextState = ConfigState.eUP;
                controls.get(playerBeingConfigured).BTN1 = keyCode;
                playerBeingConfigured++;
                
                // check to see if we've finished configuring all players
                if(playerBeingConfigured >= controls.size())
                {
                    dispose();
                }
                break;
        }
        
        currentState = nextState;
        msg.setText(getMsg());
    }
    
    private String
    getMsg()
    {
        String msgFmt = "Press player %d's \"%s\" button";
        String btnStr = "UNSUPPORTED BUTTON STATE!";
        
        switch(currentState)
        {
            case eUP:
                btnStr = "UP";
                break;
            case eDOWN:
                btnStr = "DOWN";
                break;
            case eLEFT:
                btnStr = "LEFT";
                break;
            case eRIGHT:
                btnStr = "RIGHT";
                break;
            case eBTN1:
                btnStr = "FIRE";
                break;
        }
        
        return String.format(msgFmt, playerBeingConfigured + 1, btnStr);
    }
    
    @Override
    public void
    keyTyped(
            KeyEvent ke)
    {
        // don't care
    }

    @Override
    public void
    keyPressed(
            KeyEvent ke)
    {
        configure(ke.getKeyCode());
    }

    @Override
    public void
    keyReleased(
            KeyEvent ke)
    {
        // don't care
    }
}
