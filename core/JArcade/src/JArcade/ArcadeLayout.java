/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JArcade;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author daniel
 */
public class ArcadeLayout extends FlowLayout {
    private double ar;
    boolean sideBySide;
    
    public ArcadeLayout() {
        ar = 3.0/4.0;
        sideBySide = true;
    }

    @Override
    public void layoutContainer(Container cntnr) {
        super.layoutContainer(cntnr);
        double cntnrAR = getAR(cntnr);
        int numPanels = cntnr.getComponentCount();
        
        double gridAR = ar;
        int gridX;
        int gridY;
        if (sideBySide) {
            // the compLocs are placed horizontally
            gridAR *= numPanels;
            gridX = numPanels;
            gridY = 1;
        } else {
            // the compLocs are placed vertically
            gridAR /= numPanels;
            gridX = 1;
            gridY = numPanels;
        }
        
        Dimension newDim;
        if (cntnrAR < gridAR) {
            // container is taller than component
            newDim = getDimFromWidth(ar, cntnr.getWidth() / gridX);
        } else {
            // container is wider than component
            newDim = getDimFromHeight(ar, cntnr.getHeight() / gridY);
        }
        
        int gridBaseX = cntnr.getWidth() / 2 - gridX * newDim.width / 2;
        int gridBaseY = cntnr.getHeight() / 2 - gridY * newDim.height / 2;
        int i = 0;
        for (Component comp : cntnr.getComponents()) {
            comp.setSize(newDim);

            int newX, newY;
            if (sideBySide) {
                newX = gridBaseX + newDim.width * i;
                newY = gridBaseY;
            } else {
                newX = gridBaseX;
                newY = gridBaseY + newDim.height * i;
            }
            
            comp.setLocation(newX, newY);
            i++;
        }
    }
    
    /**
     * Setter for the aspect ratio
     * @param aspectRatio the aspect ratio for the individual compLocs
     */
    public void setAspectRatio(double aspectRatio) {
        ar = aspectRatio;
    }
    
    /**
     * Compute the components aspect ratio
     * @param c the component
     * @return the component's aspect ratio (width/height)
     */
    private double getAR(Component c) {
        return (double)c.getWidth() / c.getHeight();
    }
    
    private Dimension getDimFromHeight(double ar, int height) {
        return new Dimension((int)(ar*height),height);
    }
    
    private Dimension getDimFromWidth(double ar, int width) {
        return new Dimension(width,(int)(width/ar));
    }
    
    public static void main(String[] args) {
        JFrame f = new JFrame();
        f.setLayout(new ArcadeLayout());
        f.setBackground(Color.BLACK);
        
        JButton b0 = new JButton("0");
        JButton b1 = new JButton("1");
        
        f.add(b0);
        f.add(b1);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
        f.pack();
    }
}
