package JArcade;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;

public class JarClassLoader extends MultiClassLoader {

    private JarResources jarResources;

    public JarClassLoader(String jarName) {
        // Create the JarResource and suck in the .jar file.
        jarResources = new JarResources(jarName);
    }

    @Override
    public InputStream getResourceAsStream(String string) {
        byte[] res = jarResources.getResource(string);
        return new ByteArrayInputStream(res);
    }

    protected byte[] loadClassBytes(String className) {
        // Support the MultiClassLoader's class name munging facility.
        className = formatClassName(className);

        // Attempt to get the class data from the JarResource.
        return (jarResources.getResource(className));
    }

}	// End of Class JarClassLoader.
